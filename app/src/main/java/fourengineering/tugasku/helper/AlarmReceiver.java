package fourengineering.tugasku.helper;


import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.util.List;

import fourengineering.tugasku.R;
import fourengineering.tugasku.activity.MainActivity2;
import fourengineering.tugasku.activity.SettingActivity;
import fourengineering.tugasku.adapter.Tugas;


public class AlarmReceiver extends WakefulBroadcastReceiver {
    AlarmManager mAlarmManager;
    PendingIntent mPendingIntent;
    int mReminderCount;
    long milSecond = 1000;
    long milMinute = 60000;
    long milHour = 3600000;
    long milDay = 86400000;
    long milWeek = 604800000;
    long hasilPerban;
    String title2 = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        //int mReceivedID = Integer.parseInt(intent.getStringExtra(ReminderEditActivity.EXTRA_REMINDER_ID));
        int mReceivedID = Integer.parseInt(intent.getStringExtra(SettingActivity.EXTRA_REMINDER_ID));

        /*ReminderDatabase rb = new ReminderDatabase(context);
        Reminder reminder = rb.getReminder(mReceivedID);
        String mTitle = reminder.getTitle();
        String mDateDeadline = reminder.getDateDeadline();
        String mTimeDeadline = reminder.getTimeDeadline();
        boolean stat;*/

        /*Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try{
            date = sdf.parse(mDateDeadline + " " + mTimeDeadline);
        } catch (ParseException e){
            e.printStackTrace();
        }

        Calendar c3 = Calendar.getInstance();
        long currentTime2 = c3.getTimeInMillis();
        long diffTime3 = (currentTime2 - date.getTime()) * -1;
        if (diffTime3 >= milSecond && diffTime3 <= milMinute) {
            hasilPerban = diffTime3/milSecond;
            title2 = ("Sisa Waktu Pengerjaan " + hasilPerban + " Detik Lagi");
        } else if (diffTime3>= milMinute && diffTime3 <= milHour) {
            hasilPerban = diffTime3/milMinute;
            title2 = ("Sisa Waktu Pengerjaan " + hasilPerban + " Menit Lagi");
        } else if (diffTime3 >= milHour && diffTime3 <= milDay) {
            hasilPerban = diffTime3/milHour;
            title2 = ("Sisa Waktu Pengerjaan " + hasilPerban + " Jam Lagi");
        } else if (diffTime3 >= milDay && diffTime3 <= milWeek) {
            hasilPerban = diffTime3/milDay;
            title2 = ("Sisa Waktu Pengerjaan " + hasilPerban + " Hari Lagi");
        } else {
            title2 = "SELESAI, SILAHKAN KUMPULKAN TUGAS ANDA!";
        }*/

        /*Intent editIntent = new Intent(context, ReminderEditActivity.class);
        editIntent.putExtra(ReminderEditActivity.EXTRA_REMINDER_ID, Integer.toString(mReceivedID));
        PendingIntent mClick = PendingIntent.getActivity(context, mReceivedID, editIntent, PendingIntent.FLAG_UPDATE_CURRENT);*/

        TugasDatabase rb = new TugasDatabase(context);
        List<Tugas> mTest = rb.getAllTugas();

        mReminderCount = mTest.size();

        Intent editIntent = new Intent(context, MainActivity2.class);
        editIntent.putExtra(SettingActivity.EXTRA_REMINDER_ID, Integer.toString(mReceivedID));
        PendingIntent mClick = PendingIntent.getActivity(context, mReceivedID, editIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_tugasku)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setTicker("Kamu Memiliki " + mReminderCount + " Tugas")
                .setContentText("Kamu Memiliki " + mReminderCount + " Tugas")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(mClick)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true);

        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(mReceivedID, mBuilder.build());
    }

    public void setAlarm(Context context, int ID) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(SettingActivity.EXTRA_REMINDER_ID, Integer.toString(ID));
        mPendingIntent = PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        /*Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;*/

        mAlarmManager.set(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime(),
                mPendingIntent);

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    //public void setRepeatAlarm(Context context, Calendar calendar, long calendar2, int ID, long RepeatTime) {
    public void setRepeatAlarm(Context context, int ID, long RepeatTime) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);
        //intent.putExtra(ReminderEditActivity.EXTRA_REMINDER_ID, Integer.toString(ID));
        intent.putExtra(SettingActivity.EXTRA_REMINDER_ID, Integer.toString(ID));
        mPendingIntent = PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //mPendingIntent = PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        /*Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;

        Calendar c2 = Calendar.getInstance();
        long currentTime2 = c2.getTimeInMillis();
        long diffTime2 = currentTime2 - calendar2;*/

        //if(diffTime2 < 0) {
            mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    RepeatTime, mPendingIntent);
        //} else if (diffTime2 > 0){
            //mAlarmManager.cancel(mPendingIntent);
        //}
        //}

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void cancelAlarm(Context context, int ID) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        mPendingIntent = PendingIntent.getBroadcast(context, ID, new Intent(context, AlarmReceiver.class), 0);
        mAlarmManager.cancel(mPendingIntent);

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}