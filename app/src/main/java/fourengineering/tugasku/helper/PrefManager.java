package fourengineering.tugasku.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Naufal on 02/07/2017.
 */

public class PrefManager {
    private static final String PREV_NAME = "Setting_Pref";
    private static final String KEY_REPEAT_TYPE = "repeat_type";
    private static final String KEY_REPEAT_NO = "repeat_NO";
    private static final String KEY_REPEAT = "repeat";
    private static final String KEY_ACTIVE = "active";
    private static final String KEY_MODE = "mode";
    private static final int PRIVATE_MODE = 0;
    private static final String REMINDER_ID = "reminder_id";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private final SharedPreferences pref;

    public PrefManager(Context context) {
        pref = context.getSharedPreferences(PREV_NAME, PRIVATE_MODE);
    }

    public void setRepeat(String Repeat) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_REPEAT, Repeat);
        editor.apply();
    }

    public String getRepeat() {
        return pref.getString(KEY_REPEAT, Constant.DEFAULT_REPEAT);
    }

    public void setRepeatType(String RepeatType) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_REPEAT_TYPE, RepeatType);
        editor.apply();
    }

    public String getRepeatType() {
        return pref.getString(KEY_REPEAT_TYPE, Constant.DEFAULT_REPEAT_TYPE);
    }

    public void setRepeatNo(String RepeatNo) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_REPEAT_NO, RepeatNo);
        editor.apply();
    }

    public String getRepeatNo() {
        return pref.getString(KEY_REPEAT_NO, Constant.DEFAULT_REPEAT_NO);
    }

    public void setActive(String Active) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_ACTIVE, Active);
        editor.apply();
    }

    public String getActive() {
        return pref.getString(KEY_ACTIVE, Constant.DEFAULT_ACTIVE);
    }

    public void setMode(String mode) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_MODE, mode);
        editor.apply();
    }

    public String getMode() {
        return pref.getString(KEY_MODE, Constant.DEFAULT_MODE);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    private class Constant {
        public static final String DEFAULT_REPEAT_TYPE = "Menit";
        public static final String DEFAULT_REPEAT_NO = "1";
        public static final String DEFAULT_REPEAT = "true";
        public static final String DEFAULT_ACTIVE = "true";
        public static final String DEFAULT_REMINDERID = "1";
        public static final String DEFAULT_MODE = "online";
    }
}
