package fourengineering.tugasku.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.List;

import fourengineering.tugasku.adapter.Tugas;


public class BootReceiver extends BroadcastReceiver {

    private String mTitle;
    private String mTime;
    private String mTimeDeadline;
    private String mDate;
    private String mDateDeadline;
    private String mRepeatNo;
    private String mRepeatType;
    private String mVoice;
    private String mPhoto;
    private String mActive;
    private String mRepeat;
    private String mType;
    private String[] mDateSplit;
    private String[] mDateDeadlineSplit;
    private String[] mTimeSplit;
    private String[] mTimeDeadlineSplit;
    private TugasDatabase rb;
    private int mYear, mMonth, mHour, mMinute, mDay, mReceivedID;
    private int mYear2, mMonth2, mHour2, mMinute2, mDay2;
    private long mRepeatTime;

    private Calendar mCalendar, mCalendar2, mCalendar3;
    private AlarmReceiver mAlarmReceiver;

    private static final long milMinute = 60000L;
    private static final long milHour = 3600000L;
    private static final long milDay = 86400000L;
    private static final long milWeek = 604800000L;
    private static final long milMonth = 2592000000L;


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

            PrefManager pm = new PrefManager(context);
            rb = new TugasDatabase(context);
            mAlarmReceiver = new AlarmReceiver();
            List<Tugas> mTest = rb.getAllTugas();
            //int a = rb.getRemindersCount();

            if (!mTest.isEmpty()) {
                mRepeat = pm.getRepeat();
                mRepeatNo = pm.getRepeatNo();
                mRepeatType = pm.getRepeatType();
                mActive = pm    .getActive();
                //mReceivedID = Integer.parseInt(pm.getReminderID());

                ProccessReminder pr = new ProccessReminder();
                pr.ProccessReminder(context);

                /*if (mRepeatType.equals("Menit")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milMinute;
                } else if (mRepeatType.equals("Jam")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milHour;
                } else if (mRepeatType.equals("Hari")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milDay;
                } else if (mRepeatType.equals("Minggu")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milWeek;
                } else if (mRepeatType.equals("Bulan")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milMonth;
                }

                if (mActive.equals("true")) {
                    if (mRepeat.equals("true")) {
                        mAlarmReceiver.setRepeatAlarm(context, mReceivedID, mRepeatTime);
                    } else if (mRepeat.equals("false")) {
                        mAlarmReceiver.setAlarm(context, 1);
                    }
                }*/
            } else if (mTest.isEmpty()){
                mAlarmReceiver.cancelAlarm(context, 1);
            }
            /*ReminderDatabase rb = new ReminderDatabase(context);
            mCalendar = Calendar.getInstance();
            mCalendar2 = Calendar.getInstance();
            mCalendar3 = Calendar.getInstance();
            mAlarmReceiver = new AlarmReceiver();

            List<Reminder> reminders = rb.getAllReminders();

            for (Reminder rm : reminders) {
                mReceivedID = rm.getID();
                mRepeat = rm.getRepeat();
                mRepeatNo = rm.getRepeatNo();
                mRepeatType = rm.getRepeatType();
                mVoice = rm.getVoice();
                mPhoto = rm.getPhoto();
                mActive = rm.getActive();
                mDate = rm.getDate();
                mDateDeadline = rm.getDateDeadline();
                mTime = rm.getTime();
                mTimeDeadline = rm.getTimeDeadline();
                mType = rm.getType();

                mDateSplit = mDate.split("/");
                mDateDeadlineSplit = mDateDeadline.split("/");
                mTimeSplit = mTime.split(":");

                mDay = Integer.parseInt(mDateSplit[0]);
                mMonth = Integer.parseInt(mDateSplit[1]);
                mYear = Integer.parseInt(mDateSplit[2]);
                mHour = Integer.parseInt(mTimeSplit[0]);
                mMinute = Integer.parseInt(mTimeSplit[1]);*/

                /*mDay2 = Integer.parseInt(mDateDeadlineSplit[0]);
                mMonth2 = Integer.parseInt(mDateDeadlineSplit[1]);
                mYear2 = Integer.parseInt(mDateDeadlineSplit[2]);
                mHour2 = Integer.parseInt(mTimeDeadlineSplit[0]);
                mMinute2 = Integer.parseInt(mTimeDeadlineSplit[1]);*/

                /*mCalendar.set(Calendar.MONTH, --mMonth);
                mCalendar.set(Calendar.YEAR, mYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, mDay);
                mCalendar.set(Calendar.HOUR_OF_DAY, mHour);
                mCalendar.set(Calendar.MINUTE, mMinute);
                mCalendar.set(Calendar.SECOND, 0);*/

                /*mCalendar2.set(Calendar.MONTH, --mMonth2);
                mCalendar2.set(Calendar.YEAR, mYear2);
                mCalendar2.set(Calendar.DAY_OF_MONTH, mDay2);
                mCalendar2.set(Calendar.HOUR_OF_DAY, mHour2);
                mCalendar2.set(Calendar.MINUTE, mMinute2);
                mCalendar2.set(Calendar.SECOND, 0);*/

                /*Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try{
                    date2 = sdf.parse(mDateDeadline + " " + mTimeDeadline);
                } catch (ParseException e){
                    e.printStackTrace();
                }

                //long diffTime = mCalendar3.getTimeInMillis() - date2.getTime();

                if (mRepeatType.equals("Minute")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milMinute;
                } else if (mRepeatType.equals("Hour")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milHour;
                } else if (mRepeatType.equals("Day")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milDay;
                } else if (mRepeatType.equals("Week")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milWeek;
                } else if (mRepeatType.equals("Month")) {
                    mRepeatTime = Integer.parseInt(mRepeatNo) * milMonth;
                }

                if (mActive.equals("true")) {
                    if (mRepeat.equals("true")) {
                        mAlarmReceiver.setRepeatAlarm(context, mCalendar, date2.getTime(), mReceivedID, mRepeatTime);
                    } else if (mRepeat.equals("false")) {
                        mAlarmReceiver.setAlarm(context, mCalendar, mReceivedID);
                    }
                }
            }*/
        }
    }
}