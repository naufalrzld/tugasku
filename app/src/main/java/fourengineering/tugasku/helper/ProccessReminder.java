package fourengineering.tugasku.helper;

import android.content.Context;

/**
 * Created by Naufal on 18/04/2016.
 */
public class ProccessReminder {
    private String mRepeat;
    private String mRepeatNo;
    private String mRepeatType;
    private String mActive;
    private PrefManager pm;
    private AlarmReceiver mAlarmReceiver;
    private long mRepeatTime;
    private static final long milMenit = 60000L;
    private static final long milJam = 3600000L;
    private static final long milHari = 86400000L;
    private static final long milMinggu = 604800000L;

    public void ProccessReminder(Context context) {
        pm = new PrefManager(context);
        mRepeat = pm.getRepeat();
        mRepeatNo = pm.getRepeatNo();
        mRepeatType = pm.getRepeatType();
        mActive = pm.getActive();
        mAlarmReceiver = new AlarmReceiver();

        if (mRepeatType.equals("Menit")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milMenit;
        } else if (mRepeatType.equals("Jam")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milJam;
        } else if (mRepeatType.equals("Hari")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milHari;
        } else if (mRepeatType.equals("Minggu")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milMinggu;
        }

        if (mActive.equals("true")) {
            if (mRepeat.equals("true")) {
                //new AlarmReceiver().setRepeatAlarm(getApplicationContext(), mCalendar, date2.getTime(), ID, mRepeatTime);
                mAlarmReceiver.setRepeatAlarm(context, 1, mRepeatTime);
            } else if (mRepeat.equals("false")) {
                new AlarmReceiver().setAlarm(context, 1);
            }
        } else if (mActive.equals("false")) {
            mAlarmReceiver.cancelAlarm(context, 1);
        }
        //Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
    }
}
