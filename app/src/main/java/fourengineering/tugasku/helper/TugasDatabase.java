package fourengineering.tugasku.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fourengineering.tugasku.adapter.Tugas;


public class TugasDatabase extends SQLiteOpenHelper {
    private static final String TAG = TugasDatabase.class.getSimpleName();
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "TugasKuDB";
    private static final String TABLE_REMINDERS = "tugasku_table";
    private static final String TABLE_SHARED_REMINDER = "shared_tugas_table";
    private static final String TABLE_USER = "user";
    private static final String TABLE_OFFLINE = "tugasku_offline_table";

    private static final String KEY_ID = "id";
    private static final String KEY_REMINDERID = "reminder_id";
    private static final String KEY_MEMBERID = "member_id";
    private static final String KEY_FROM = "from_member";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESC = "desc";
    private static final String KEY_DATE = "date";
    private static final String KEY_DATE_DEADLINE = "date_deadline";
    private static final String KEY_TIME = "time";
    private static final String KEY_TIME_DEADLINE = "time_deadline";
    private static final String KEY_SYNC = "sync";
    private static final String KEY_UPDATE = "updated";

    private static final String KEY_IDUSR = "id";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_NOTLP = "no_tlp";
    private static final String KEY_UID = "uid";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_TOKEN = "token";

    public TugasDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_REMINDERS_TABLE = "CREATE TABLE " + TABLE_REMINDERS +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_REMINDERID + " INTEGER,"
                + KEY_MEMBERID + " INTEGER,"
                + KEY_TITLE + " TEXT,"
                + KEY_DESC + " TEXT,"
                + KEY_DATE + " TEXT,"
                + KEY_DATE_DEADLINE + " TEXT,"
                + KEY_TIME + " INTEGER,"
                + KEY_TIME_DEADLINE + " INTEGER,"
                + KEY_SYNC + " INTEGER,"
                + KEY_UPDATE + " INTEGER" + ")";
        db.execSQL(CREATE_REMINDERS_TABLE);

        String CREATE_REMINDER_SHARED_TABLE = "CREATE TABLE " + TABLE_SHARED_REMINDER +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_REMINDERID + " INTEGER,"
                + KEY_FROM + " TEXT,"
                + KEY_TITLE + " TEXT,"
                + KEY_DESC + " TEXT,"
                + KEY_DATE + " TEXT,"
                + KEY_DATE_DEADLINE + " TEXT,"
                + KEY_TIME + " INTEGER,"
                + KEY_TIME_DEADLINE + " INTEGER" + ")";
        db.execSQL(CREATE_REMINDER_SHARED_TABLE);

        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_IDUSR + " INTEGER PRIMARY KEY,"
                + KEY_USERNAME + " TEXT,"
                + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE,"
                + KEY_PASSWORD + " TEXT,"
                + KEY_NOTLP + " TEXT,"
                + KEY_UID + " INTEGER,"
                + KEY_CREATED_AT + " TEXT,"
                + KEY_TOKEN + " TEXT)";
        db.execSQL(CREATE_LOGIN_TABLE);
        Log.d(TAG, "Database tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHARED_REMINDER);
        onCreate(db);
    }

    public int addTugas(Tugas tugas){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_REMINDERID , tugas.getReminderID());
        values.put(KEY_MEMBERID , tugas.getMemberID());
        values.put(KEY_TITLE , tugas.getTitle());
        values.put(KEY_DESC , tugas.getDesc());
        values.put(KEY_DATE , tugas.getDate());
        values.put(KEY_DATE_DEADLINE , tugas.getDateDeadline());
        values.put(KEY_TIME , tugas.getTime());
        values.put(KEY_TIME_DEADLINE , tugas.getTimeDeadline());
        values.put(KEY_SYNC , tugas.getSync());
        values.put(KEY_UPDATE , tugas.getUpdate());
        long ID = db.insert(TABLE_REMINDERS, null, values);
        db.close();
        Log.d(TAG, "New Task Insert into sqlite: " +ID);
        return (int) ID;
    }

    public int addSharedTugas(Tugas tugas){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_REMINDERID , tugas.getReminderID());
        values.put(KEY_FROM , tugas.getFrom());
        values.put(KEY_TITLE , tugas.getTitle());
        values.put(KEY_DESC , tugas.getDesc());
        values.put(KEY_DATE , tugas.getDate());
        values.put(KEY_DATE_DEADLINE , tugas.getDateDeadline());
        values.put(KEY_TIME , tugas.getTime());
        values.put(KEY_TIME_DEADLINE , tugas.getTimeDeadline());
        long ID = db.insert(TABLE_SHARED_REMINDER, null, values);
        db.close();
        Log.d(TAG, "New Insert into sqlite: " +ID);
        return (int) ID;
    }

    public void addUser(String username, String name, String email, String password, String notlp, int uid, String token, String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, username); // Name
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_PASSWORD, password); // PASSWORD
        values.put(KEY_NOTLP, notlp); // NoTlp
        values.put(KEY_UID, uid); // User ID
        values.put(KEY_TOKEN, token); // Token
        values.put(KEY_CREATED_AT, created_at); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection
        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public void updateMember(String username, String name, String email, String notlp) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, username);
        values.put(KEY_NAME, name);
        values.put(KEY_EMAIL, email);
        values.put(KEY_NOTLP, notlp);

        long id = db.update(TABLE_USER, values, KEY_USERNAME + "=?", new String[]{username});
        Log.d(TAG, "Update Member in SQLite: " + id);
    }

    public void updatePassword(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PASSWORD, password);

        long id = db.update(TABLE_USER, values, KEY_USERNAME + "=?", new String[]{username});
        Log.d(TAG, "Update Password in SQLite: " + id);
    }

    public int updateTugas(Tugas tugas){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_REMINDERID , tugas.getReminderID());
        values.put(KEY_MEMBERID , tugas.getMemberID());
        values.put(KEY_TITLE , tugas.getTitle());
        values.put(KEY_DESC , tugas.getDesc());
        values.put(KEY_DATE , tugas.getDate());
        values.put(KEY_DATE_DEADLINE , tugas.getDateDeadline());
        values.put(KEY_TIME , tugas.getTime());
        values.put(KEY_TIME_DEADLINE , tugas.getTimeDeadline());
        values.put(KEY_SYNC , tugas.getSync());
        values.put(KEY_UPDATE , tugas.getUpdate() + 1);

        return db.update(TABLE_REMINDERS, values, KEY_ID + "=?",
                new String[]{String.valueOf(tugas.getID())});
    }

    public int updateTugasShared(Tugas tugas){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FROM , tugas.getFrom());
        values.put(KEY_TITLE , tugas.getTitle());
        values.put(KEY_DESC , tugas.getDesc());
        values.put(KEY_DATE , tugas.getDate());
        values.put(KEY_DATE_DEADLINE , tugas.getDateDeadline());
        values.put(KEY_TIME , tugas.getTime());
        values.put(KEY_TIME_DEADLINE , tugas.getTimeDeadline());

        long id = db.update(TABLE_SHARED_REMINDER, values, KEY_REMINDERID + "=?",
                new String[]{String.valueOf(tugas.getReminderID())});

        Log.d(TAG, "Update Tugas Shared: " + id);
        return (int) id;
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("username", cursor.getString(1));
            user.put("name", cursor.getString(2));
            user.put("email", cursor.getString(3));
            user.put("password", cursor.getString(4));
            user.put("no_tlp", cursor.getString(5));
            user.put("uid", cursor.getString(6));
            user.put("created_at", cursor.getString(7));
            user.put("token", cursor.getString(8));
        }
        cursor.close();
        db.close();
        // return user

        Log.d(	TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    public Tugas getTugas(int id, int reminder_id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_REMINDERS, new String[]
                        {
                                KEY_ID,
                                KEY_REMINDERID,
                                KEY_MEMBERID,
                                KEY_TITLE,
                                KEY_DESC,
                                KEY_DATE,
                                KEY_DATE_DEADLINE,
                                KEY_TIME,
                                KEY_TIME_DEADLINE,
                                KEY_SYNC,
                                KEY_UPDATE
                        }, KEY_ID + "=? OR " + KEY_REMINDERID + "=?",

                new String[] {String.valueOf(id), String.valueOf(reminder_id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Tugas tugas = new Tugas(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                Integer.parseInt(cursor.getString(2)), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), cursor.getString(8), Integer.parseInt(cursor.getString(9)),
                Integer.parseInt(cursor.getString(10)));

        return tugas;
    }

    public Tugas getTugasShared(int reminderID, int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHARED_REMINDER, new String[]
                        {
                                KEY_ID,
                                KEY_REMINDERID,
                                KEY_FROM,
                                KEY_TITLE,
                                KEY_DESC,
                                KEY_DATE,
                                KEY_DATE_DEADLINE,
                                KEY_TIME,
                                KEY_TIME_DEADLINE
                        }, KEY_ID + "=? OR " + KEY_REMINDERID + "=?",

                new String[] {String.valueOf(id), String.valueOf(reminderID)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Tugas tugas = new Tugas(Integer.parseInt(cursor.getString(0)), Integer.parseInt(cursor.getString(1)),
                cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8));

        return tugas;
    }

    public List<Tugas> getAllTugas(){
        List<Tugas> tugasList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_REMINDERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Tugas tugas = new Tugas();
                tugas.setID(Integer.parseInt(cursor.getString(0)));
                tugas.setReminderID(Integer.parseInt(cursor.getString(1)));
                tugas.setMemberID(Integer.parseInt(cursor.getString(2)));
                tugas.setTitle(cursor.getString(3));
                tugas.setDesc(cursor.getString(4));
                tugas.setDate(cursor.getString(5));
                tugas.setDateDeadline(cursor.getString(6));
                tugas.setTime(cursor.getString(7));
                tugas.setTimeDeadline(cursor.getString(8));
                tugas.setSync(Integer.parseInt(cursor.getString(9)));
                tugas.setUpdate(Integer.parseInt(cursor.getString(10)));
                tugasList.add(tugas);
            } while (cursor.moveToNext());
        }
        return tugasList;
    }

    public List<Tugas> getAllTugasShared(){
        List<Tugas> tugasList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_SHARED_REMINDER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Tugas tugas = new Tugas();
                tugas.setID(Integer.parseInt(cursor.getString(0)));
                tugas.setReminderID(Integer.parseInt(cursor.getString(1)));
                tugas.setFrom(cursor.getString(2));
                tugas.setTitle(cursor.getString(3));
                tugas.setDesc(cursor.getString(4));
                tugas.setDate(cursor.getString(5));
                tugas.setDateDeadline(cursor.getString(6));
                tugas.setTime(cursor.getString(7));
                tugas.setTimeDeadline(cursor.getString(8));
                tugasList.add(tugas);
            } while (cursor.moveToNext());
        }
        return tugasList;
    }

    public int getTugasCount(){
        String countQuery = "SELECT * FROM " + TABLE_REMINDERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

    public void deleteTugas(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REMINDERS, KEY_ID + "=?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteTugasShared(Tugas tugas){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHARED_REMINDER, KEY_REMINDERID + "=?",
                new String[]{String.valueOf(tugas.getReminderID())});
        db.close();
    }

    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }

    public void deleteAllTugas() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_REMINDERS, null, null);
        db.delete(TABLE_SHARED_REMINDER, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }
}
