package fourengineering.tugasku.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.amlcurran.showcaseview.ShowcaseView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import fourengineering.tugasku.R;
import fourengineering.tugasku.activity.DetaiTugaslActivity;
import fourengineering.tugasku.activity.ListTemanActivity;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.helper.DateTimeSorter;
import fourengineering.tugasku.helper.TugasDatabase;


public class TugasFragment extends Fragment {

    private static final String TAG = TugasFragment.class.getSimpleName();

    private RecyclerView mList;
    private SimpleAdapter mAdapter;
    private TextView mNoReminderView;
    private ImageView mIcadd;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private TugasDatabase tb;
    private MultiSelector mMultiSelector = new MultiSelector();
    //private AlarmReceiver mAlarmReceiver;
    private ShowcaseView sv;
    private ProgressDialog pDialog;
    private String username;
    private Tugas reminder = new Tugas();
    private String mTitle;
    private int mTempPost;
    //CircleImageView mTitleType;

    public TugasFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_one, container, false);

        pDialog = new ProgressDialog(v.getContext());
        pDialog.setCancelable(false);

        mList = (RecyclerView) v.findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) v.findViewById(R.id.no_reminder_text);
        mIcadd = (ImageView) v.findViewById(R.id.ic_add);

        mIcadd.setImageResource(R.drawable.ic_no_task);

        tb = new TugasDatabase(v.getContext());

        List<Tugas> test = tb.getAllTugasShared();

        if (test.isEmpty()) {
            mNoReminderView.setText("Tidak Ada Tugas Yang Tersimpan");
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        }

        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new SimpleAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        return v;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.menu_long, menu);
    }

    private android.support.v7.view.ActionMode.Callback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            getActivity().getMenuInflater().inflate(R.menu.menu_long, menu);
            mMultiSelector.clearSelections();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            super.onDestroyActionMode(actionMode);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.delete_tugas:
                    actionMode.finish();
                    for (int i = IDmap.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            int id = IDmap.get(i);
                            Tugas temp = tb.getTugas(id, 0);
                            int tugas_id = temp.getReminderID();

                            tb.deleteTugas(tugas_id);
                            mAdapter.removeItemSelected(i);
                        }
                    }

                    mMultiSelector.clearSelections();
                    mAdapter.onDeleteItem(getDefaultItemCount());

                    List<Tugas> mTest = tb.getAllTugas();

                    if (mTest.isEmpty()) {
                        mNoReminderView.setVisibility(View.VISIBLE);
                        mIcadd.setVisibility(View.VISIBLE);
                    } else {
                        mNoReminderView.setVisibility(View.GONE);
                        mIcadd.setVisibility(View.GONE);
                    }

                    return true;

                default:
                    break;
            }
            return false;
        }
    };

    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);
        Intent i = new Intent(getContext(), DetaiTugaslActivity.class);
        i.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, mStringClickID);
        i.putExtra("action", 2);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.setItemCount(getDefaultItemCount());
    }

    @Override
    public void onResume() {
        super.onResume();

        List<Tugas> mTest = tb.getAllTugasShared();

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        } else {
            mNoReminderView.setVisibility(View.GONE);
            mIcadd.setVisibility(View.GONE);
        }

        mAdapter.setItemCount(getDefaultItemCount());
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.VerticalItemHolder> {
        public ArrayList<ReminderItem> mItems;

        long milSecond = 1000;
        long milMinute = 60000;
        long milHour = 3600000;
        long milDay = 86400000;
        long milWeek = 604800000;
        long milMonth = 2592000000L;
        long hasilPerban, hasilPerban2;

        public SimpleAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
        }

        public void removeItemSelected(int selected) {
            if (mItems.isEmpty()) return;
            mItems.remove(selected);
            notifyItemRemoved(selected);
        }

        @Override
        public VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_item_shared, container, false);

            return new VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime);
            itemHolder.setReminderFrom(item.mFrom);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class ReminderItem {
            public String mFrom;
            public String mTitle;
            public String mDateTime;

            public ReminderItem(String From, String Title, String DateTime) {
                this.mFrom = From;
                this.mTitle = Title;
                this.mDateTime = DateTime;
            }
        }

        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter) a).getDateTime();
                String o2 = ((DateTimeSorter) b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        public class VerticalItemHolder extends SwappingHolder
                implements View.OnClickListener, View.OnLongClickListener {
            private TextView mTitleText, mFromText, mDateAndTimeText, mCountDown;
            private ImageView mThumbnailImage;
            //private ImageView mActiveImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private SimpleAdapter mAdapter;
            private CheckBox mCB;

            public VerticalItemHolder(View itemView, SimpleAdapter adapter) {
                super(itemView, mMultiSelector);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(false);

                mAdapter = adapter;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mFromText = (TextView) itemView.findViewById(R.id.info_share);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mCountDown = (TextView) itemView.findViewById(R.id.recycle_countdown);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
                mCB = (CheckBox) itemView.findViewById(R.id.checkbox);

            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    mTempPost = mList.getChildAdapterPosition(v);

                    int mReminderClickID = IDmap.get(mTempPost);
                    selectReminder(mReminderClickID);

                } else if (mMultiSelector.getSelectedPositions().isEmpty()) {
                    mAdapter.setItemCount(getDefaultItemCount());
                }
            }

            @Override
            public boolean onLongClick(View v) {
                ((AppCompatActivity)getActivity()).startSupportActionMode(mDeleteMode);
                mMultiSelector.setSelectable(true);
                mMultiSelector.setSelected(this, true);
                return true;
            }

            public void setReminderFrom(String from) {
                mFromText.setText("Dari " + from);
            }

            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if (title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            public void setReminderDateTime(String datetime) {
                Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try {
                    date2 = sdf.parse(datetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                long perbandingan = (c.getTimeInMillis() - date2.getTime()) * -1;
                if (perbandingan >= milSecond && perbandingan <= milMinute) {
                    hasilPerban = perbandingan / milSecond;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Detik");
                } else if (perbandingan >= milMinute && perbandingan <= milHour) {
                    hasilPerban = perbandingan / milMinute;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Menit");
                } else if (perbandingan >= milHour && perbandingan <= milDay) {
                    hasilPerban = perbandingan / milHour;
                    hasilPerban2 = perbandingan % milHour;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Jam " + hasilPerban2 / milMinute + " Menit");
                } else if (perbandingan >= milDay && perbandingan <= milWeek) {
                    hasilPerban = perbandingan / milDay;
                    hasilPerban2 = perbandingan % milDay;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Hari " + hasilPerban2 / milHour + " Jam");
                } else if (perbandingan >= milWeek && perbandingan <= milMonth){
                    hasilPerban = perbandingan / milWeek;
                    hasilPerban2 = perbandingan % milWeek;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Minggu " + hasilPerban2 / milDay + " Hari");
                } else if (perbandingan >= milMonth) {
                    hasilPerban = perbandingan / milMonth;
                    hasilPerban2 = perbandingan % milMonth;
                    mDateAndTimeText.setText(datetime + "");
                    if (hasilPerban2 >= milDay && hasilPerban2 <= milWeek) {
                        mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milDay + " Hari");
                    } else if (hasilPerban2 >= milWeek && hasilPerban2 <= milMonth) {
                        long hasilPerban3 = hasilPerban2 % milWeek;
                        if (hasilPerban3 >= milDay) {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu " + hasilPerban3 / milDay + " Hari");
                        } else {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu");
                        }
                    } else {
                        mCountDown.setText(" " + hasilPerban + " Bulan");
                    }
                } else {
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText("SELESAI!");
                }
            }
        }

        //testing data dami
        public ReminderItem generateDummyData() {
            return new ReminderItem("1", "2", "3");
        }

        public List<ReminderItem> generateData(int count) {
            ArrayList<ReminderItem> items = new ArrayList<>();

            List<Tugas> tugas = tb.getAllTugasShared();
            List<String> From = new ArrayList<>();
            List<String> Titles = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList = new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();

            for (Tugas r : tugas) {
                From.add(r.getFrom());
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDateDeadline() + " " + r.getTimeDeadline());
                IDList.add(r.getID());
            }

            int key = 0;
            for (int k = 0; k < Titles.size(); k++) {
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            for (DateTimeSorter item : DateTimeSortList) {
                int i = item.getIndex();

                items.add(new ReminderItem(From.get(i), Titles.get(i), DateAndTime.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }
}
