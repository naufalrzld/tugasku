package fourengineering.tugasku.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.activity.DetaiTugaslActivity;
import fourengineering.tugasku.activity.ListTemanActivity;
import fourengineering.tugasku.activity.MainActivity2;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.AlarmReceiver;
import fourengineering.tugasku.helper.DateTimeSorter;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.SessionManager;
import fourengineering.tugasku.helper.TugasDatabase;

/**
 * Created by Naufal on 17/07/2017.
 */

public class MainFragment extends Fragment {

    private RecyclerView mList;
    private TugasAdapter mAdapter;

    private SessionManager session;
    private Toolbar mToolbar;
    private SwipeRefreshLayout refreshLayout;
    private FloatingActionButton fabAdd;
    private TextView mNoReminderView, lblName, lblEmail;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;
    private MultiSelector mMultiSelector = new MultiSelector();
    private int mTempPost;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private TugasDatabase tb;
    private PrefManager pm;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ImageView profilImage, mIcadd;

    private FirebaseAuth mAuth;

    private String name = null;
    private String email= null;
    private String member_id;
    private String username;
    private String mode;
    private int action;

    private ProgressDialog pDialog;
    private AlarmReceiver mAlarmReceiver;

    private boolean status_delete;

    private static final String TAG = MainFragment.class.getSimpleName();
    
    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_to_refresh);
        fabAdd = (FloatingActionButton) v.findViewById(R.id.fabAdd);
        mList = (RecyclerView) v.findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) v.findViewById(R.id.no_reminder_text);
        mIcadd = (ImageView) v.findViewById(R.id.ic_add);

        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);

        tb = new TugasDatabase(getContext());
        mAlarmReceiver = new AlarmReceiver();

        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new TugasAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        HashMap<String, String> user = tb.getUserDetails();
        username = user.get("username");
        member_id = user.get("uid");

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                        getReminderList(username);
                    }
                });
                getReminderList(username);
            }
        });

        refreshLayout.setColorSchemeResources(R.color.green_600);

        return v;
    }

    private android.support.v7.view.ActionMode.Callback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            getActivity().getMenuInflater().inflate(R.menu.menu_long, menu);
            mMultiSelector.clearSelections();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            super.onDestroyActionMode(actionMode);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.delete_tugas:
                    actionMode.finish();
                    for (int i = IDmap.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            int id = IDmap.get(i);
                            Tugas temp = tb.getTugas(id, 0);
                            int reminder_id = temp.getReminderID();

                            String id_reminder = String.valueOf(reminder_id);
                            deleteTugas(id_reminder, temp.getID(), i);
                            //deleteReminder(id_reminder, temp.getID(), i);
                            //tb.deleteTugas(temp);
                            //mAdapter.removeItemSelected(i);
                            //mAlarmReceiver.cancelAlarm(getApplicationContext(), id);
                        }
                    }

                    mMultiSelector.clearSelections();
                    //mAdapter.onDeleteItem(getDefaultItemCount());

                    /*List<Tugas> mTest = tb.getAllTugas();

                    if (mTest.isEmpty()) {
                        mNoReminderView.setVisibility(View.VISIBLE);
                        mIcadd.setVisibility(View.VISIBLE);
                        mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
                    } else {
                        mNoReminderView.setVisibility(View.GONE);
                        mIcadd.setVisibility(View.GONE);
                        ProccessReminder pr = new ProccessReminder();
                        pr.ProccessReminder(getApplicationContext());
                    }*/

                    if (status_delete) {
                        Toast.makeText(getContext(), "Terhapus", Toast.LENGTH_SHORT).show();
                    }

                    return true;

                case R.id.share_tugas:
                    int id;
                    ArrayList<Integer> id_reminder = new ArrayList<Integer>();
                    for (int i = IDmap.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            id = IDmap.get(i);
                            Tugas r = tb.getTugas(id, 0);
                            int reminder_id = r.getReminderID();
                            id_reminder.add(reminder_id);
                        }
                    }
                    Intent in = new Intent(getContext(), ListTemanActivity.class);
                    in.putIntegerArrayListExtra("ID", id_reminder);
                    startActivity(in);
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                default:
                    break;
            }
            return false;
        }
    };

    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);
        Intent i = new Intent(getContext(), DetaiTugaslActivity.class);
        i.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, mStringClickID);
        i.putExtra("action", 1);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.setItemCount(getDefaultItemCount());
    }

    @Override
    public void onResume(){
        super.onResume();

        List<Tugas> mTest = tb.getAllTugas();

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        } else {
            mNoReminderView.setVisibility(View.GONE);
            mIcadd.setVisibility(View.GONE);
            //ProccessReminder pr = new ProccessReminder();
            //pr.ProccessReminder(getApplicationContext());
        }

        mAdapter.setItemCount(getDefaultItemCount());
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    public class TugasAdapter extends RecyclerView.Adapter<TugasAdapter.VerticalItemHolder> {
        private ArrayList<TugasAdapter.ReminderItem> mItems;

        long milSecond = 1000;
        long milMinute = 60000;
        long milHour = 3600000;
        long milDay = 86400000;
        long milWeek = 604800000;
        long milMonth = 2592000000L;
        long hasilPerban, hasilPerban2;

        public TugasAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
        }

        public void removeItemSelected(int selected) {
            if (!mItems.isEmpty()) {
                //mAdapter.notifyDataSetChanged();
                mItems.remove(selected);
                notifyItemRemoved(selected);
            }
        }

        @Override
        public TugasAdapter.VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_items, container, false);

            return new TugasAdapter.VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(TugasAdapter.VerticalItemHolder itemHolder, int position) {
            TugasAdapter.ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public  class ReminderItem {
            public String mTitle;
            public String mDateTime;

            public ReminderItem(String Title, String DateTime) {
                this.mTitle = Title;
                this.mDateTime = DateTime;
            }
        }

        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter)a).getDateTime();
                String o2 = ((DateTimeSorter)b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        public class VerticalItemHolder extends SwappingHolder
                implements View.OnClickListener, View.OnLongClickListener {
            private TextView mTitleText, mDateAndTimeText, mCountDown;
            //private TextView mRepeatInfoText;
            private ImageView mThumbnailImage;
            //private ImageView mActiveImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private TugasAdapter mAdapter;
            //private CircleImageView civ;

            public VerticalItemHolder(View itemView, TugasAdapter adapter) {
                super(itemView, mMultiSelector);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(true);

                mAdapter = adapter;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mCountDown = (TextView) itemView.findViewById(R.id.recycle_countdown);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    mTempPost = mList.getChildAdapterPosition(v);

                    int mReminderClickID = IDmap.get(mTempPost);
                    selectReminder(mReminderClickID);

                } else if(mMultiSelector.getSelectedPositions().isEmpty()){
                    mAdapter.setItemCount(getDefaultItemCount());
                }
            }

            @Override
            public boolean onLongClick(View v) {
                ((AppCompatActivity)getActivity()).startSupportActionMode(mDeleteMode);
                mMultiSelector.setSelectable(true);
                mMultiSelector.setSelected(this, true);
                return true;
            }

            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if(title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            public void setReminderDateTime(String datetime) {
                Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try{
                    date2 = sdf.parse(datetime);
                } catch (ParseException e){
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                long perbandingan = (c.getTimeInMillis() - date2.getTime()) * -1;

                if (perbandingan >= milSecond && perbandingan <= milMinute) {
                    hasilPerban = perbandingan/milSecond;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Detik");
                } else if (perbandingan >= milMinute && perbandingan <= milHour) {
                    hasilPerban = perbandingan/milMinute;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Menit");
                } else if (perbandingan >= milHour && perbandingan <= milDay) {
                    hasilPerban = perbandingan/milHour;
                    hasilPerban2 = perbandingan%milHour;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Jam " + hasilPerban2/milMinute + " Menit");
                } else if (perbandingan >= milDay && perbandingan <= milWeek) {
                    hasilPerban = perbandingan / milDay;
                    hasilPerban2 = perbandingan % milDay;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Hari " + hasilPerban2 / milHour + " Jam");
                } else if (perbandingan >= milWeek && perbandingan <= milMonth){
                    hasilPerban = perbandingan / milWeek;
                    hasilPerban2 = perbandingan % milWeek;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Minggu " + hasilPerban2 / milDay + " Hari");
                } else if (perbandingan >= milMonth) {
                    hasilPerban = perbandingan / milMonth;
                    hasilPerban2 = perbandingan % milMonth;
                    mDateAndTimeText.setText(datetime + "");
                    if (hasilPerban2 >= milDay && hasilPerban2 <= milWeek) {
                        mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milDay + " Hari");
                    } else if (hasilPerban2 >= milWeek && hasilPerban2 <= milMonth) {
                        long hasilPerban3 = hasilPerban2 % milWeek;
                        if (hasilPerban3 >= milDay) {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu " + hasilPerban3 / milDay + " Hari");
                        } else {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu");
                        }
                    } else {
                        mCountDown.setText(" " + hasilPerban + " Bulan");
                    }
                } else {
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText("SELESAI!");
                }
            }
        }

        //testing data dami
        public ReminderItem generateDummyData() {
            return new ReminderItem("1", "2");
        }

        public List<ReminderItem> generateData(int count) {
            ArrayList<ReminderItem> items = new ArrayList<>();

            List<Tugas> tugas = tb.getAllTugas();
            List<String> Titles = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList= new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();

            for (Tugas r : tugas) {
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDateDeadline() + " " + r.getTimeDeadline());
                IDList.add(r.getID());
            }

            int key = 0;
            for(int k = 0; k<Titles.size(); k++){
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            for (DateTimeSorter item:DateTimeSortList) {
                int i = item.getIndex();

                items.add(new TugasAdapter.ReminderItem(Titles.get(i), DateAndTime.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }

    private void deleteTugas(String reminder_id, int id, int pos) {
        if (mode.equals("online")) {
            deleteReminder(reminder_id, id, pos);
        } else if (mode.equals("offline")) {
            tb.deleteTugas(id);
            mAdapter.removeItemSelected(pos);
            //mAdapter.onDeleteItem(getDefaultItemCount());

            List<Tugas> mTest = tb.getAllTugas();

            if (mTest.isEmpty()) {
                mNoReminderView.setVisibility(View.VISIBLE);
                mIcadd.setVisibility(View.VISIBLE);
                mAlarmReceiver.cancelAlarm(getContext(), 1);
            } else {
                mNoReminderView.setVisibility(View.GONE);
                mIcadd.setVisibility(View.GONE);
                ProccessReminder pr = new ProccessReminder();
                pr.ProccessReminder(getContext());
            }
        }
    }

    private void saveToLocal(int reminder_id, int member_id, String title, String desc, String date, String date_deadline, String time, String time_deadline, int updated) {
        int ID;
        ID = tb.addTugas(new Tugas(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated));
        if (ID == 1 || ID > 1) {
            ProccessReminder pr = new ProccessReminder();
            pr.ProccessReminder(getContext());
        }
    }

    private void getReminderList(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    List<Tugas> tugas = tb.getAllTugas();
                    List<Integer> id = new ArrayList<>();

                    // Check for error node in json
                    if (!error) {
                        List<Integer> updatedLocal = new ArrayList<>();
                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                id.add(t.getReminderID());
                                updatedLocal.add(t.getUpdate());
                            }
                        }
                        JSONArray tugas_arr = jObj.getJSONArray("reminder_list");
                        int l_tugas = tugas_arr.length();

                        for (int i=0; i<l_tugas; i++) {
                            JSONObject c = tugas_arr.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            if (i < id.size()) {
                                if (id.get(i) == reminder_id) {
                                    if (updatedLocal.get(i) > updated) {
                                        updateReminder(tugas.get(i), String.valueOf(reminder_id));
                                    }
                                } else {
                                    saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                                }
                                if (id.get(i) != reminder_id){
                                    saveToServer(tugas.get(i), String.valueOf(member_id));
                                }
                            } else {
                                saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                            }
                            if (l_tugas < id.size() && i == l_tugas-1) {
                                for (int j = i+1; j<id.size(); j++) {
                                    saveToServer(tugas.get(j), String.valueOf(member_id));
                                }
                            }
                        }

                        List<Tugas> tugas2 = tb.getAllTugas();
                        if (tugas2.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            mAdapter.setItemCount(getDefaultItemCount());
                        }

                        tugas.clear();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                saveToServer(t, member_id);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                refreshLayout.setRefreshing(false);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
                refreshLayout.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void deleteReminder(final String reminder_id, final int id, final int pos) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Menghapus Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Delete Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        status_delete = true;

                        String successmsg = jObj.getString("success_msg");
                        tb.deleteTugas(id);
                        mAdapter.removeItemSelected(pos);
                        mAdapter.onDeleteItem(getDefaultItemCount());

                        List<Tugas> mTest = tb.getAllTugas();

                        if (mTest.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                            mAlarmReceiver.cancelAlarm(getContext(), 1);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            ProccessReminder pr = new ProccessReminder();
                            pr.ProccessReminder(getContext());
                        }

                        //Toast.makeText(getApplicationContext(), successmsg, Toast.LENGTH_LONG).show();
                    } else {
                        status_delete = false;
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                status_delete = false;
                Log.e(TAG, "Delete Tugas Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", reminder_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveToServer(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_reminder";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Save To Server Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        int reminder_id= jObj.getInt("reminder_id");

                        JSONObject user = jObj.getJSONObject("reminder");
                        int member_id = user.getInt("member_id");

                        Tugas t = tb.getTugas(tugas.getID(), 0);
                        t.setReminderID(reminder_id);
                        t.setMemberID(member_id);
                        tb.updateTugas(t);
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Save To Server Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", member_id);
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateReminder(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Tugas Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Sync Success");
                    } else {
                        // Error in login. Get the error message
                        /*String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();*/

                        Log.e(TAG, "Sync Error");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Tugas Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", String.valueOf(tugas.getReminderID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("member_id", member_id);
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
