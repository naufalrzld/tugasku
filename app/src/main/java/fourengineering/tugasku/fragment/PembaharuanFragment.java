package fourengineering.tugasku.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.amlcurran.showcaseview.ShowcaseView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.activity.DetaiTugaslActivity;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.DateTimeSorter;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.TugasDatabase;

import static android.app.Activity.RESULT_OK;


public class PembaharuanFragment extends Fragment {
    private static final String TAG = PembaharuanFragment.class.getSimpleName();

    private RecyclerView mList;
    private SimpleAdapter mAdapter;
    private TextView mNoReminderView;
    private ImageView mIcadd;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private TugasDatabase tb;
    private MultiSelector mMultiSelector = new MultiSelector();
    private ShowcaseView sv;
    private ProgressDialog pDialog;
    private String username, uid;
    private Tugas reminder = new Tugas();
    private String mTitle;
    private int mTempPost;

    private PrefManager pm;
    private String mode;
    private boolean isConnected;

    private List<Tugas> tugasKu = new ArrayList<>();

    public PembaharuanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_one, container, false);

        pDialog = new ProgressDialog(v.getContext());
        pDialog.setCancelable(false);

        mList = (RecyclerView) v.findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) v.findViewById(R.id.no_reminder_text);
        mIcadd = (ImageView) v.findViewById(R.id.ic_add);

        pm = new PrefManager(getContext());
        tb = new TugasDatabase(v.getContext());
        isConnected = ConnectivityReceiver.isConnected();

        if (tugasKu.isEmpty()) {
            mNoReminderView.setText("Tidak Ada Pembaharuan");
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        }

        mode = pm.getMode();
        if (mode.equals("online")) {
            mIcadd.setImageResource(R.drawable.ic_no_update_task);
            HashMap<String, String> user = tb.getUserDetails();
            username = user.get("username");
            uid = user.get("uid");

            if (isConnected) {
                getReminderListShared(username);
            }
        } else {
            mIcadd.setImageResource(R.drawable.ic_no_internet);
            mNoReminderView.setText("Sedang dalam mode offline!");
        }

        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new SimpleAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        return v;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.menu_long, menu);
    }

    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);
        Intent i = new Intent(getContext(), DetaiTugaslActivity.class);
        i.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, mStringClickID);
        i.putExtra("action", 6);
        i.putExtra("pos", mClickID);
        i.putExtra("tugas_id", tugasKu.get(mClickID).getReminderID());
        i.putExtra("from", tugasKu.get(mClickID).getFrom());
        i.putExtra("title", tugasKu.get(mClickID).getTitle());
        i.putExtra("desc", tugasKu.get(mClickID).getDesc());
        i.putExtra("date", tugasKu.get(mClickID).getDate());
        i.putExtra("date_deadline", tugasKu.get(mClickID).getDateDeadline());
        i.putExtra("time", tugasKu.get(mClickID).getTime());
        i.putExtra("time_deadline", tugasKu.get(mClickID).getTimeDeadline());
        startActivityForResult(i, 5);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mAdapter.setItemCount(getDefaultItemCount());
        if (requestCode == 5) {
            if (resultCode == RESULT_OK) {
                getReminderListShared(username);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tugasKu.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        } else {
            mNoReminderView.setVisibility(View.GONE);
            mIcadd.setVisibility(View.GONE);
        }

        mAdapter.setItemCount(getDefaultItemCount());
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.VerticalItemHolder> {
        public List<ReminderItem> mItems;

        long milSecond = 1000;
        long milMinute = 60000;
        long milHour = 3600000;
        long milDay = 86400000;
        long milWeek = 604800000;
        long milMonth = 2592000000L;
        long hasilPerban, hasilPerban2;

        public SimpleAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void removeItemSelected(int selected) {
            mAdapter.notifyDataSetChanged();
            mItems.remove(selected);
            notifyItemRemoved(selected);
        }

        @Override
        public SimpleAdapter.VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_item_shared, container, false);

            return new SimpleAdapter.VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime);
            itemHolder.setReminderFrom(item.mFrom);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class ReminderItem {
            public String mFrom;
            public String mTitle;
            public String mDateTime;

            public ReminderItem(String From, String Title, String DateTime) {
                this.mFrom = From;
                this.mTitle = Title;
                this.mDateTime = DateTime;
            }
        }

        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter) a).getDateTime();
                String o2 = ((DateTimeSorter) b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        public class VerticalItemHolder extends SwappingHolder
                implements View.OnClickListener, View.OnLongClickListener {
            private Button btnSync;
            private TextView mTitleText, mFromText, mDateAndTimeText, mCountDown;
            private ImageView mThumbnailImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private SimpleAdapter mAdapter;
            private CheckBox mCB;

            public VerticalItemHolder(final View itemView, SimpleAdapter adapter) {
                super(itemView, mMultiSelector);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(false);

                mAdapter = adapter;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mFromText = (TextView) itemView.findViewById(R.id.info_share);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mCountDown = (TextView) itemView.findViewById(R.id.recycle_countdown);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
                mCB = (CheckBox) itemView.findViewById(R.id.checkbox);
                btnSync = (Button) itemView.findViewById(R.id.btnSync);

                btnSync.setVisibility(View.VISIBLE);

                btnSync.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = IDmap.get(getAdapterPosition());
                        int tugas_id = tugasKu.get(pos).getReminderID();
                        String from = tugasKu.get(pos).getFrom();
                        String title = tugasKu.get(pos).getTitle();
                        String desc = tugasKu.get(pos).getDesc();
                        String date = tugasKu.get(pos).getDate();
                        String date_deadline = tugasKu.get(pos).getDateDeadline();
                        String time = tugasKu.get(pos).getTime();
                        String time_deadline = tugasKu.get(pos).getTimeDeadline();

                        tb.updateTugasShared(new Tugas(tugas_id, from, title, desc, date, date_deadline, time, time_deadline));
                        syncTugas(uid, String.valueOf(tugas_id));
                    }
                });

            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    mTempPost = mList.getChildAdapterPosition(v);

                    int mReminderClickID = IDmap.get(mTempPost);
                    selectReminder(mReminderClickID);

                } else if (mMultiSelector.getSelectedPositions().isEmpty()) {
                    mAdapter.setItemCount(getDefaultItemCount());
                }
            }

            @Override
            public boolean onLongClick(View v) {
                return false;
            }

            public void setReminderFrom(String from) {
                mFromText.setText("Dari " + from);
            }

            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if (title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            public void setReminderDateTime(String datetime) {
                Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try {
                    date2 = sdf.parse(datetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                long perbandingan = (c.getTimeInMillis() - date2.getTime()) * -1;
                if (perbandingan >= milSecond && perbandingan <= milMinute) {
                    hasilPerban = perbandingan / milSecond;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Detik");
                } else if (perbandingan >= milMinute && perbandingan <= milHour) {
                    hasilPerban = perbandingan / milMinute;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Menit");
                } else if (perbandingan >= milHour && perbandingan <= milDay) {
                    hasilPerban = perbandingan / milHour;
                    hasilPerban2 = perbandingan % milHour;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Jam " + hasilPerban2 / milMinute + " Menit");
                } else if (perbandingan >= milDay && perbandingan <= milWeek) {
                    hasilPerban = perbandingan / milDay;
                    hasilPerban2 = perbandingan % milDay;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Hari " + hasilPerban2 / milHour + " Jam");
                } else if (perbandingan >= milWeek && perbandingan <= milMonth){
                    hasilPerban = perbandingan / milWeek;
                    hasilPerban2 = perbandingan % milWeek;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Minggu " + hasilPerban2 / milDay + " Hari");
                } else if (perbandingan >= milMonth) {
                    hasilPerban = perbandingan / milMonth;
                    hasilPerban2 = perbandingan % milMonth;
                    mDateAndTimeText.setText(datetime + "");
                    if (hasilPerban2 >= milDay && hasilPerban2 <= milWeek) {
                        mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milDay + " Hari");
                    } else if (hasilPerban2 >= milWeek && hasilPerban2 <= milMonth) {
                        long hasilPerban3 = hasilPerban2 % milWeek;
                        if (hasilPerban3 >= milDay) {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu " + hasilPerban3 / milDay + " Hari");
                        } else {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu");
                        }
                    } else {
                        mCountDown.setText(" " + hasilPerban + " Bulan");
                    }
                } else {
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText("SELESAI!");
                }
            }
        }

        //testing data dami
        public ReminderItem generateDummyData() {
            return new ReminderItem("1", "2", "3");
        }

        public List<ReminderItem> generateData(int count) {
            List<ReminderItem> items = new ArrayList<>();

            List<String> From = new ArrayList<>();
            List<String> Titles = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList = new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();

            for (Tugas r : tugasKu) {
                From.add(r.getFrom());
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDateDeadline() + " " + r.getTimeDeadline());
                IDList.add(r.getID());
            }

            int key = 0;
            for (int k = 0; k < Titles.size(); k++) {
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            for (DateTimeSorter item : DateTimeSortList) {
                int i = item.getIndex();

                items.add(new ReminderItem(From.get(i), Titles.get(i), DateAndTime.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }

    private void getReminderListShared(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        tugasKu.clear();
                        JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                        for (int i = 0; i < reminder_list.length(); i++) {
                            JSONObject c = reminder_list.getJSONObject(i);

                            int reminder_id = c.getInt("reminder_id");
                            String from = c.getString("name");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");

                            tugasKu.add(new Tugas(i, reminder_id, from, title, desc, date, date_deadline, time, time_deadline));
                        }
                        onResume();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        tugasKu.clear();
                        onResume();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("saved", String.valueOf(1));
                params.put("synced", String.valueOf(0));

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void syncTugas(final String member_id, final String tugas_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Menyimpan Data ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_TUGAS_SHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    /*int a = reminder_list.length() - test.size();
                    selisih.add(String.valueOf(a));
                    Toast.makeText(getApplicationContext(), selisih.get(0), Toast.LENGTH_LONG).show();*/

                    //if (test.size() < reminder_list.length()) {
                    // Check for error node in json
                    if (!error) {
                        JSONObject reminder = jObj.getJSONObject("reminder");

                        int reminder_id = reminder.getInt("reminder_id");
                        if (reminder_id != 0) {
                            Toast.makeText(getContext(), "Diperbaharui", Toast.LENGTH_SHORT).show();
                            getReminderListShared(username);
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);
                params.put("tugas_id", tugas_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
