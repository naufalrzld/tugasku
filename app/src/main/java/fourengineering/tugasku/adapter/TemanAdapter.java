package fourengineering.tugasku.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import fourengineering.tugasku.R;

/**
 * Created by Naufal on 23/03/2016.
 */
public class TemanAdapter extends ArrayAdapter<Teman> {
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;
    private SparseBooleanArray mSelectedItemsIds = new SparseBooleanArray();

    boolean checkbox = false;

    public TemanAdapter(Context context, ArrayList<Teman> arrayTeman, boolean checkbox) {
        super(context, 0, arrayTeman);
        this.checkbox = checkbox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Teman teman = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_teman, parent, false);
        }

        TextView lblNama = (TextView) convertView.findViewById(R.id.labelName);
        TextView lblEmail = (TextView) convertView.findViewById(R.id.lblEmail);
        CheckBox cbTeman = (CheckBox) convertView.findViewById(R.id.cbTeman);
        ImageView imgTeman = (ImageView) convertView.findViewById(R.id.imgTeman);

        if (checkbox == false) cbTeman.setVisibility(View.GONE);

        lblNama.setText(teman.name);
        lblEmail.setText(teman.email);

        String letter = "A";
        String title = lblNama.getText().toString();

        if(title != null && !title.isEmpty()) {
            letter = title.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();
        mDrawableBuilder = TextDrawable.builder()
                .buildRound(letter, color);
        imgTeman.setImageDrawable(mDrawableBuilder);

        return convertView;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
}
