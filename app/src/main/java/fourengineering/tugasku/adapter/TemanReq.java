package fourengineering.tugasku.adapter;

/**
 * Created by Naufal on 12/06/2016.
 */
public class TemanReq {
    public String member_id;
    public String name;
    public String email;

    public TemanReq(String member_id, String name, String email){
        this.member_id = member_id;
        this.name = name;
        this.email = email;
    }
}
