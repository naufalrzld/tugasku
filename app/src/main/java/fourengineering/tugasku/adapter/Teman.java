package fourengineering.tugasku.adapter;

/**
 * Created by Naufal on 23/03/2016.
 */
public class Teman {
    public String friend_list_id;
    public String member_id;
    public String username;
    public String name;
    public String email;

    public Teman(String friend_list_id, String member_id, String username, String name, String email){
        this.friend_list_id = friend_list_id;
        this.member_id = member_id;
        this.username = username;
        this.name = name;
        this.email = email;
    }
}
