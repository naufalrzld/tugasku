package fourengineering.tugasku.adapter;

public class Tugas {
    private int mID;
    private int mReminderID;
    private int mMemberID;
    private String mFrom;
    private String mTitle;
    private String mDesc;
    private String mDate;
    private String mDateDeadline;
    private String mTime;
    private String mTimeDeadline;
    private int mSync;
    private int mUpdate;

    public Tugas(int ID, int ReminderID, int MemberID, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline, int sync, int update){
        mID = ID;
        mReminderID = ReminderID;
        mMemberID = MemberID;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
        mSync = sync;
        mUpdate = update;
    }

    public Tugas(int ID, int ReminderID, String From, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline){
        mID = ID;
        mReminderID = ReminderID;
        mFrom = From;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
    }

    public Tugas(int ReminderID, int MemberID, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline, int update){
        mReminderID = ReminderID;
        mMemberID = MemberID;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
        mUpdate = update;
    }

    public Tugas(int ReminderID, String From, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline){
        mReminderID = ReminderID;
        mFrom = From;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
    }

    public Tugas(int MemberID, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline, int update){
        mMemberID = MemberID;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
        mUpdate = update;
    }

    /*

    ||  OFFLINE  ||

     */
    public Tugas(String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline){
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
    }

    public Tugas(int ID, String Title, String Desc, String Date, String DateDeadline, String Time, String TimeDeadline){
        mID = ID;
        mTitle = Title;
        mDesc = Desc;
        mDate = Date;
        mDateDeadline = DateDeadline;
        mTime = Time;
        mTimeDeadline = TimeDeadline;
    }

    public Tugas(){}

    public int getID() {
        return mID;
    }

    public void setID(int ID) {
        mID = ID;
    }

    public int getReminderID() {
        return mReminderID;
    }

    public void setReminderID(int ReminderID) {
        mReminderID = ReminderID;
    }

    public int getMemberID() {
        return mMemberID;
    }

    public void setMemberID(int MemberID) {
        mMemberID = MemberID;
    }

    public String getFrom() {
        return mFrom;
    }

    public void setFrom(String From) {
        mFrom = From;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String Desc) {
        mDesc = Desc;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateDeadline() {
        return mDateDeadline;
    }

    public void setDateDeadline(String dateDeadline) {mDateDeadline = dateDeadline;}

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {mTime = time;}

    public String getTimeDeadline() {
        return mTimeDeadline;
    }

    public void setTimeDeadline(String timeDeadline) { mTimeDeadline = timeDeadline; }

    public int getSync() {
        return mSync;
    }

    public void setSync(int sync) {
        mSync = sync;
    }

    public int getUpdate() {
        return mUpdate;
    }

    public void setUpdate(int update) { mUpdate = update; }

}
