package fourengineering.tugasku.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import fourengineering.tugasku.R;

/**
 * Created by Naufal on 12/06/2016.
 */
public class TemanReqAdapter extends ArrayAdapter<TemanReq> {
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;

    public TemanReqAdapter(Context context, ArrayList<TemanReq> arrayTemanReq) {
        super(context, 0, arrayTemanReq);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TemanReq temanReq = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_teman_req, parent, false);
        }

        TextView lblNama = (TextView) convertView.findViewById(R.id.labelName);
        TextView lblEmail = (TextView) convertView.findViewById(R.id.lblEmail);
        ImageView imgTeman = (ImageView) convertView.findViewById(R.id.imgTeman);

        lblNama.setText(temanReq.name);
        lblEmail.setText(temanReq.email);

        String letter = "A";
        String title = lblNama.getText().toString();

        if (title != null && !title.isEmpty()) {
            letter = title.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();
        mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
        imgTeman.setImageDrawable(mDrawableBuilder);

        return convertView;
    }
}
