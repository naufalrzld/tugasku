package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.TugasDatabase;

public class AddTugasKuActivity extends AppCompatActivity implements
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private Toolbar mToolbar;

    private EditText mTitleText, mDescText;
    private TextView mDateText, mDateDeadlineText,  mTimeText, mTimeDeadlineText;

    private Calendar kalender, mCalendar, mCalendar2;
    private int mYear, mMonth, mHour, mMinute, mDay;
    private int mYear2, mMonth2, mHour2, mMinute2, mDay2;
    private String mTitle, mDesc, mTime, mTimeDeadline, mDate, mDateDeadline;
    private int member_id, mUpdate = 0;
    private int stat = 0;
    private String mode;
    private ProgressDialog pDialog;

    private PrefManager pm;
    private TugasDatabase tb;

    Date sekarang = new Date();

    private static final String KEY_TITLE = "title_key";
    private static final String KEY_DESC = "desc_key";
    private static final String KEY_TIME = "time_key";
    private static final String KEY_TIME_DEADLINE = "time_deadline_key";
    private static final String KEY_DATE = "date_key";
    private static final String KEY_DATE_DEADLINE = "date_deadline_key";

    private static final String TAG = AddTugasKuActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tugas_ku);

        kalender = Calendar.getInstance();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitleText = (EditText) findViewById(R.id.tugasku_title);
        mDescText = (EditText) findViewById(R.id.tugasku_desc);
        mDateText = (TextView) findViewById(R.id.set_date);
        mDateDeadlineText = (TextView) findViewById(R.id.set_date_deadline);
        mTimeText = (TextView) findViewById(R.id.set_time);
        mTimeDeadlineText = (TextView) findViewById(R.id.set_time_deadline);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_activity_add_reminder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tb = new TugasDatabase(getApplicationContext());
        pm = new PrefManager(this);

        mode = pm.getMode();

        if (mode.equals("online")) {
            checkConnection();
            HashMap<String, String> user = tb.getUserDetails();
            member_id = Integer.parseInt(user.get("uid"));
        }

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        mCalendar = Calendar.getInstance();
        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = mCalendar.get(Calendar.MINUTE);
        mYear = mCalendar.get(Calendar.YEAR);
        mMonth = mCalendar.get(Calendar.MONTH) + 1;
        mDay = mCalendar.get(Calendar.DATE);

        mCalendar2 = Calendar.getInstance();
        mHour2 = mCalendar2.get(Calendar.HOUR_OF_DAY);
        mMinute2 = mCalendar2.get(Calendar.MINUTE);
        mYear2 = mCalendar2.get(Calendar.YEAR);
        mMonth2 = mCalendar2.get(Calendar.MONTH) + 1;
        mDay2 = mCalendar2.get(Calendar.DATE);

        mDate = mDay + "/" + mMonth + "/" + mYear;
        mTime = mHour + ":" + mMinute;
        mDateDeadline = mDay2 + "/" + mMonth2 + "/" + mYear2;
        mTimeDeadline = mHour2 + ":" + mMinute2;

        mTitleText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTitle = s.toString().trim();
                mTitleText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mDescText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDesc = s.toString().trim();
                mDescText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDateText.setText(mDate);
        mTimeText.setText(mTime);

        if (savedInstanceState != null) {
            String savedTitle = savedInstanceState.getString(KEY_TITLE);
            mTitleText.setText(savedTitle);
            mTitle = savedTitle;

            String savedDesc = savedInstanceState.getString(KEY_DESC);
            mDescText.setText(savedDesc);
            mDesc = savedDesc;

            String savedTime = savedInstanceState.getString(KEY_TIME);
            mTimeText.setText(savedTime);
            mTime = savedTime;

            String savedTimeDeadline = savedInstanceState.getString(KEY_TIME_DEADLINE);
            mTimeDeadlineText.setText(savedTimeDeadline);
            mTimeDeadline = savedTimeDeadline;

            String savedDate = savedInstanceState.getString(KEY_DATE);
            mDateText.setText(savedDate);
            mDate = savedDate;

            String savedDateDeadline = savedInstanceState.getString(KEY_DATE_DEADLINE);
            mDateDeadlineText.setText(savedDateDeadline);
            mDateDeadline = savedDateDeadline;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(KEY_TITLE, mTitleText.getText());
        outState.putCharSequence(KEY_DESC, mDescText.getText());
        outState.putCharSequence(KEY_TIME, mTimeText.getText());
        outState.putCharSequence(KEY_TIME_DEADLINE, mTimeDeadlineText.getText());
        outState.putCharSequence(KEY_DATE, mDateText.getText());
        outState.putCharSequence(KEY_DATE_DEADLINE, mDateDeadlineText.getText());
    }

    public void setTime(View v){
        stat = 0;
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void setTime2(View v){
        stat = 1;
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void setDate(View v){
        stat = 0;
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void setDate2(View v){
        stat = 1;
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        mHour = hourOfDay;
        mMinute = minute;
        mHour2 = hourOfDay;
        mMinute2 = minute;
        if (stat == 0) {
            if (minute < 10) {
                mTime = hourOfDay + ":" + "0" + minute;
            } else {
                mTime = hourOfDay + ":" + minute;
            }
            mTimeText.setText(mTime);
        } else {
            if (minute < 10) {
                mTimeDeadline = hourOfDay + ":" + "0" + minute;
            } else {
                mTimeDeadline = hourOfDay + ":" + minute;
            }
            mTimeDeadlineText.setText(mTimeDeadline);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear ++;
        if(stat == 0) {
            mDay = dayOfMonth;
            mMonth = monthOfYear;
            mYear = year;
            mDate = dayOfMonth + "/" + monthOfYear + "/" + year;
            mDateText.setText(mDate);
        } else {
            mDay2 = dayOfMonth;
            mMonth2 = monthOfYear;
            mYear2 = year;
            mDateDeadline = dayOfMonth + "/" + monthOfYear + "/" + year;
            mDateDeadlineText.setText(mDateDeadline);
        }
    }

    private void saveReminder() {
        int ID = 0;
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (mode.equals("online")) {
            if (isConnected) {
                saveToServer(new Tugas(member_id, mTitle, mDesc, mDate, mDateDeadline, mTime, mTimeDeadline, mUpdate));
            }
        } else if (mode.equals("offline")) {
            ID = tb.addTugas(new Tugas(0, 0, mTitle, mDesc, mDate, mDateDeadline, mTime, mTimeDeadline, mUpdate));
            Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        if (ID >= 1) {
            ProccessReminder pr = new ProccessReminder();
            pr.ProccessReminder(getApplicationContext());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_reminder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.save_reminder:
                mTitleText.setText(mTitle);
                mDescText.setText(mDesc);

                if (mTitleText.getText().toString().length() == 0) {
                    mTitleText.setError("Judul Tidak Boleh Kosong!");

                } else if (mDescText.getText().toString().length() == 0) {
                    mDescText.setError("Deskripsi Tidak Boleh Kosong");
                } else {
                    boolean isConnected = ConnectivityReceiver.isConnected();
                    if (!isConnected) {
                        showSnack(isConnected);
                    }
                    saveReminder();
                }
                return true;

            case R.id.discard_reminder:
                Toast.makeText(getApplicationContext(), "Batal",
                        Toast.LENGTH_SHORT).show();

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.time_deadline), message, Snackbar.LENGTH_INDEFINITE).setAction("MODE OFFLINE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tb.deleteUsers();
                    pm.setMode("offline");
                    mode = "offline";
                    Toast.makeText(getApplicationContext(), "MODE OFFLINE AKTIF", Toast.LENGTH_SHORT).show();
                }
            });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    private void saveToServer(final Tugas tugas) {
        // Tag used to cancel the request
        String tag_string_req = "req_reminder";

        pDialog.setMessage("Menyimpan ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Reminder Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        int reminder_id= jObj.getInt("reminder_id");

                        JSONObject user = jObj.getJSONObject("reminder");
                        int member_id = user.getInt("member_id");
                        String title = user.getString("title");
                        String desc = user.getString("desc");
                        String date = user.getString("date");
                        String date_deadline = user.getString("date_deadline");
                        String time = user.getString("time");
                        String time_deadline = user.getString("time_deadline");
                        int updated = user.getInt("updated");

                        tb.addTugas(new Tugas(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated));

                        Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Adding Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi tidak stabil", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", String.valueOf(tugas.getMemberID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
