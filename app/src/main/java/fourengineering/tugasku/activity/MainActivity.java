package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.fragment.MainFragment;
import fourengineering.tugasku.fragment.PembaharuanFragment;
import fourengineering.tugasku.fragment.TugasBaruFragment;
import fourengineering.tugasku.fragment.TugasFragment;
import fourengineering.tugasku.helper.AlarmReceiver;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.SessionManager;
import fourengineering.tugasku.helper.TugasDatabase;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextDrawable mDrawableBuilder;
    private TextView lblName, lblEmail;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private ImageView profilImage;

    private List<String> toolbarTitle = new ArrayList<>();

    private int[] tabIcons = {
            R.drawable.ic_my_task,
            R.drawable.ic_saved_task,
            R.drawable.ic_new_task,
            R.drawable.ic_update_task,
    };

    private TugasDatabase tb;
    private PrefManager pm;
    private ProgressDialog pDialog;
    private SessionManager session;
    private AlarmReceiver mAlarmReceiver;

    private FirebaseAuth mAuth;

    private String name = null;
    private String email= null;
    private String member_id;
    private String username;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_bar);
        checkConnection();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbar.setTitle(toolbarTitle.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        lblName = (TextView) findViewById(R.id.labelName);
        lblEmail = (TextView) findViewById(R.id.email);

        session = new SessionManager(getApplicationContext());
        pm = new PrefManager(this);
        tb = new TugasDatabase(getApplicationContext());
        mAlarmReceiver = new AlarmReceiver();

        mAuth = FirebaseAuth.getInstance();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        HashMap<String, String> user = tb.getUserDetails();

        name = user.get("name");
        username = user.get("username");
        email = user.get("email");
        member_id = user.get("uid");

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) {
                    drawerLayout.closeDrawers();
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(false);
                    drawerLayout.closeDrawers();
                }

                Intent i;
                switch (menuItem.getItemId()) {
                    case R.id.profile:
                        i = new Intent(MainActivity.this, ProfilActivity.class);
                        i.putExtra("nama", name);
                        i.putExtra("email", email);
                        startActivity(i);
                        return true;
                    case R.id.list_teman:
                        i = new Intent(MainActivity.this, ListTemanActivity.class);
                        startActivity(i);
                        return true;
                    case R.id.shared:
                        i = new Intent(MainActivity.this, ShareTugasActivity2.class);
                        startActivity(i);
                        return true;
                    case R.id.tentang:
                        showAbout();
                        return true;
                    case R.id.Setting:
                        startActivity(new Intent(MainActivity.this, SettingActivity.class));
                        return true;
                    case R.id.offonmode:
                        tb.deleteUsers();
                        pm.setMode("offline");
                        startActivity(new Intent(MainActivity.this, SplashScreen.class));
                        finish();
                        return true;
                    case R.id.logout:
                        HashMap<String, String> user = tb.getUserDetails();
                        String username = user.get("username");
                        resetToken(username);
                        tb.deleteUsers();
                        List<Tugas> mTest = tb.getAllTugas();
                        List<Tugas> mTest1 = tb.getAllTugasShared();
                        if (!mTest.isEmpty() || !mTest1.isEmpty()) {
                            tb.deleteAllTugas();
                            mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
                        }
                        mAuth.signOut();
                        logoutUser();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
                        return true;
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        lblName.setText(name);
        lblEmail.setText(email);

        String letter = "A";

        if(name != null && !name.isEmpty()) {
            letter = name.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();
        mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
        profilImage = (ImageView) findViewById(R.id.profile_image);
        profilImage.setImageDrawable(mDrawableBuilder);

        if (getIntent().getStringExtra("status").equals("login")) {
            getReminderList(username);
            //syncTugas(username);
            getReminderListShared(username);
        }
    }

    private void showAbout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_about, null);
        dialog.setIcon(R.drawable.ic_info_24dp);
        dialog.setTitle("Tentang Aplikasi");
        dialog.setView(dialogView);
        dialog.setCancelable(false);
        dialog.setPositiveButton("Rate", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }).setNegativeButton("Tutup ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Action for "Tutup".
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MainFragment(), "TugasKu");
        adapter.addFrag(new TugasFragment(), "Tugas Lain");
        adapter.addFrag(new TugasBaruFragment(), "Tugas Baru");
        adapter.addFrag(new PembaharuanFragment(), "Pembaharuan");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.fabAdd), message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            toolbarTitle.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveToLocal(int reminder_id, int member_id, String title, String desc, String date, String date_deadline, String time, String time_deadline, int updated) {
        int ID;
        ID = tb.addTugas(new Tugas(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated));
        if (ID == 1 || ID > 1) {
            ProccessReminder pr = new ProccessReminder();
            pr.ProccessReminder(getApplicationContext());
        }
    }

    private void getReminderList(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    List<Tugas> tugas = tb.getAllTugas();
                    List<Integer> id = new ArrayList<>();

                    // Check for error node in json
                    if (!error) {
                        List<Integer> updatedLocal = new ArrayList<>();
                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                id.add(t.getReminderID());
                                updatedLocal.add(t.getUpdate());
                            }
                        }
                        JSONArray tugas_arr = jObj.getJSONArray("reminder_list");
                        int l_tugas = tugas_arr.length();

                        for (int i=0; i<l_tugas; i++) {
                            JSONObject c = tugas_arr.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            if (i < id.size()) {
                                if (id.get(i) == reminder_id) {
                                    if (updatedLocal.get(i) > updated) {
                                        updateReminder(tugas.get(i), String.valueOf(reminder_id));
                                    }
                                } else {
                                    saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                                }
                                if (id.get(i) != reminder_id){
                                    saveToServer(tugas.get(i), String.valueOf(member_id));
                                }
                            } else {
                                saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                            }
                            if (l_tugas < id.size() && i == l_tugas-1) {
                                for (int j = i+1; j<id.size(); j++) {
                                    saveToServer(tugas.get(j), String.valueOf(member_id));
                                }
                            }
                        }
                        tugas.clear();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                saveToServer(t, member_id);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderListShared(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Shared Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    /*int a = reminder_list.length() - test.size();
                    selisih.add(String.valueOf(a));
                    Toast.makeText(getApplicationContext(), selisih.get(0), Toast.LENGTH_LONG).show();*/

                    //if (test.size() < reminder_list.length()) {
                    // Check for error node in json
                    if (!error) {
                        JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                        for (int i = 0; i < reminder_list.length(); i++) {
                            JSONObject c = reminder_list.getJSONObject(i);

                            int reminder_id = c.getInt("reminder_id");
                            String from = c.getString("name");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");

                            tb.addSharedTugas(new Tugas(reminder_id, from, title, desc, date, date_deadline, time, time_deadline));
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Shared Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("saved", String.valueOf(1));
                params.put("synced", String.valueOf(1));

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void resetToken(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        //pDialog.setMessage("Mencari ...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESET_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Reset Token Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        String successmsg = jObj.getString("success_msg");
                        //Toast.makeText(getApplicationContext(), successmsg, Toast.LENGTH_LONG).show();

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Reset Token Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveToServer(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_reminder";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Save To Server Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        int reminder_id= jObj.getInt("reminder_id");

                        JSONObject user = jObj.getJSONObject("reminder");
                        int member_id = user.getInt("member_id");

                        Tugas t = tb.getTugas(tugas.getID(), 0);
                        t.setReminderID(reminder_id);
                        t.setMemberID(member_id);
                        tb.updateTugas(t);
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Save To Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", member_id);
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateReminder(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Tugas Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Sync Success");
                    } else {
                        // Error in login. Get the error message
                        /*String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();*/

                        Log.e(TAG, "Sync Error");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", String.valueOf(tugas.getReminderID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("member_id", member_id);
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
