package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.SessionManager;
import fourengineering.tugasku.helper.SharedPrefManager;
import fourengineering.tugasku.helper.TugasDatabase;

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private Button btnLogin, btnOfflineMode;
    private Button btnLinkToRegister, btnLupas;
    private EditText inputUsername, inputPassword;
    private CheckBox lihat_pass;

    private ProgressBar progressBar;

    private SessionManager session;
    private PrefManager pm;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private TugasDatabase tb;

    private int uid;

    private static final String TAG = "LOGIN_ACTIVIRY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkConnection();

        inputUsername = (EditText) findViewById(R.id.username);
        inputPassword = (EditText) findViewById(R.id.password);
        lihat_pass = (CheckBox) findViewById(R.id.cb_lihat_pwd);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnOfflineMode = (Button) findViewById(R.id.btnOfflineMode);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);
        btnLupas = (Button) findViewById(R.id.btnLupaPassword);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        tb = new TugasDatabase(getApplicationContext());

        session = new SessionManager(getApplicationContext());
        pm = new PrefManager(getApplicationContext());

        mAuth = FirebaseAuth.getInstance();
        /*mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    session.setLogin(true);
                    startActivity(new Intent(LoginActivity.this, MainActivity2.class));
                    finish();
                }
            }
        };*/

        /*if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity2.class);
            startActivity(intent);
            finish();
        }*/

        lihat_pass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    inputPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    inputPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        btnOfflineMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertOfflineMode();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isConnected = ConnectivityReceiver.isConnected();
                if (isConnected) {
                    String token = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();
                    String username = inputUsername.getText().toString().trim();
                    final String password = inputPassword.getText().toString().trim();

                    if (TextUtils.isEmpty(username)) {
                        inputUsername.setError("Masukkan username");
                        return;
                    }

                    if (TextUtils.isEmpty(password)) {
                        inputPassword.setError("Masukkan Password");
                        return;
                    }

                    checkLogin(username, password, token);

                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    showSnack(isConnected);
                }
            }
        });

        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });

        btnLupas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, LupasPasswordActivity.class));
                finish();
            }
        });
    }

    /*@Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }*/

    private void showAlertOfflineMode() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, R.style.AlertDialogCustom));
        dialog.setCancelable(false);
        dialog.setIcon(R.drawable.ic_offline_mode);
        dialog.setTitle("MODE OFFLINE");
        dialog.setMessage(Html.fromHtml("<font color='#ffffff'>Tidak ada koenksi internet? Silahkan klik \"Lanjutkan\" bila ingin mengaktifkan mode offline!</font>"));
        dialog.setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                pm.setMode("offline");
                session.setLogin(true);
                startActivity(new Intent(LoginActivity.this, MainActivity2.class));
                finish();
            }
        }).setNegativeButton("Batal ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Action for "Cancel".
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.progressBar), message, Snackbar.LENGTH_LONG).setAction("MODE OFFLINE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertOfflineMode();
                }
            });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    private void checkLogin(final String username, final String password, final String token) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        uid = jObj.getInt("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        final String username = user.getString("username");
                        final String name = user.getString("name");
                        final String email = user.getString("email");
                        final String notlp = user.getString("no_tlp");
                        final String token = user.getString("token");
                        final String created_at = user.getString("created_at");

                        mAuth.signInWithEmailAndPassword(email, password)
                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        // If sign in fails, display a message to the user. If sign in succeeds
                                        // the auth state listener will be notified and logic to handle the
                                        // signed in user can be handled in the listener.
                                        progressBar.setVisibility(View.GONE);
                                        if (!task.isSuccessful()) {
                                            // there was an error
                                            if (password.length() < 8)  {
                                                inputPassword.setError("Password Minimal 8 Karakter!");
                                            } else {
                                                Toast.makeText(LoginActivity.this, "Login Gagal, Cek Kembali Username dan Password Anda!", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            session.setLogin(true);
                                            tb.addUser(username, name, email, password, notlp, uid, token, created_at);
                                            pm.setMode("online");
                                            Intent i = new Intent(LoginActivity.this, MainActivity2.class);
                                            i.putExtra("status", "login");
                                            startActivity(i);
                                            finish();
                                            //getReminderList(username);
                                            //getReminderListShared(username);
                                        }
                                    }
                                });
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi tidak stabil", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("token", token);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderList(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        List<Tugas> tugas = tb.getAllTugas();
                        JSONArray teman = jObj.getJSONArray("reminder_list");

                        for (int i=0; i<teman.length(); i++) {
                            JSONObject c = teman.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            if (!tugas.isEmpty()) {
                                for (int j=0; j<tugas.size(); j++) {
                                    int update = tugas.get(j).getUpdate();
                                    if (reminder_id == tugas.get(j).getReminderID() && update > updated) {
                                        updateReminder(tugas.get(j), String.valueOf(member_id));
                                    }
                                    if (tugas.get(j).getReminderID() == 0 && tugas.get(j).getMemberID() == 0) {
                                        saveToServer(tugas.get(j), member_id);
                                    }
                                    tugas.remove(j);
                                }
                            } else {
                                int ID;
                                ID = tb.addTugas(new Tugas(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated));
                                if (ID == 1 || ID > 1) {
                                    ProccessReminder pr = new ProccessReminder();
                                    pr.ProccessReminder(getApplicationContext());
                                }
                            }
                        }
                        Toast.makeText(getApplicationContext(), "Synced", Toast.LENGTH_SHORT).show();
                        tugas.clear();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderListShared(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    /*int a = reminder_list.length() - test.size();
                    selisih.add(String.valueOf(a));
                    Toast.makeText(getApplicationContext(), selisih.get(0), Toast.LENGTH_LONG).show();*/

                    //if (test.size() < reminder_list.length()) {
                    // Check for error node in json
                    if (!error) {
                        JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                        for (int i = 0; i < reminder_list.length(); i++) {
                            JSONObject c = reminder_list.getJSONObject(i);

                            int reminder_id = c.getInt("reminder_id");
                            String from = c.getString("name");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");

                            tb.addSharedTugas(new Tugas(reminder_id, from, title, desc, date, date_deadline, time, time_deadline));
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("saved", String.valueOf(1));
                params.put("synced", String.valueOf(1));

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateReminder(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Sync Success");
                    } else {
                        // Error in login. Get the error message
                        /*String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();*/

                        Log.e(TAG, "Sync Error");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", String.valueOf(tugas.getReminderID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("member_id", member_id);
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveToServer(final Tugas tugas, final int member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_reminder";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Reminder Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        int reminder_id= jObj.getInt("reminder_id");

                        JSONObject user = jObj.getJSONObject("reminder");
                        int member_id = user.getInt("member_id");

                        Tugas t = tb.getTugas(tugas.getID(), 0);
                        t.setReminderID(reminder_id);
                        t.setMemberID(member_id);
                        tb.updateTugas(t);
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Adding Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", String.valueOf(member_id));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
