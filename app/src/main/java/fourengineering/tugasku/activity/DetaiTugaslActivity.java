package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.TugasDatabase;

public class DetaiTugaslActivity extends AppCompatActivity implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,
        ConnectivityReceiver.ConnectivityReceiverListener{
    private static final String TAG = DetaiTugaslActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private EditText mTitleText, mDescText;
    private TextView mDateText, mDateDeadlineText, mTimeText, mTimeDeadlineText;
    private TextView lblTitle, lblDesc, mDateTextDetail, mDateDeadlineTextDetail, mTimeTextDetail, mTimeDeadlineTextDetail, mFromText;
    private LinearLayout lytDetail, lytEdit;
    private RelativeLayout lyt_from;
    private ScrollView lytDetailScrl, lytEditScrl;

    private String mTitle, mDesc, mTime, mTimeDeadline, mDate, mDateDeadline, mFrom;
    private int mReceivedID;
    private int mTugasID;
    private int mYear, mMonth, mHour, mMinute, mDay;
    private int mYear2, mMonth2, mHour2, mMinute2, mDay2;
    private Calendar mCalendar, kalender;
    private Tugas mReceivedTugas;
    private TugasDatabase tb;
    private String[] mDateSplit;
    private String[] mDateDeadlineSplit;
    private String[] mTimeSplit;
    private String[] mTimeDeadlineSplit;
    private int stat = 0;
    private int action;
    private String member_id, username, name;
    private boolean saved = false;
    private ProgressDialog pDialog;
    private PrefManager pm;
    private String mode;

    private List<Tugas> tugas = new ArrayList<>();

    private static final String KEY_TITLE = "title_key";
    private static final String KEY_DESC = "desc_key";
    private static final String KEY_TIME = "time_key";
    private static final String KEY_TIME_DEADLINE = "time_deadline_key";
    private static final String KEY_DATE = "date_key";
    private static final String KEY_DATE_DEADLINE = "date_deadline_key";

    public static final String EXTRA_TUGAS_ID = "Tugas_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tugas_ku);

        kalender = Calendar.getInstance();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitleText = (EditText) findViewById(R.id.tugasku_title);
        lblTitle = (TextView) findViewById(R.id.detail_tugasku_title);
        mDescText = (EditText) findViewById(R.id.tugasku_desc);
        lblDesc = (TextView) findViewById(R.id.detail_tugasku_desc);
        mDateText = (TextView) findViewById(R.id.set_date);
        mDateDeadlineText = (TextView) findViewById(R.id.set_date_deadline);
        mTimeText = (TextView) findViewById(R.id.set_time);
        mTimeDeadlineText = (TextView) findViewById(R.id.set_time_deadline);
        mFromText = (TextView) findViewById(R.id.set_from);

        lytDetail = (LinearLayout) findViewById(R.id.lyt_detail_tugas);
        lytEdit = (LinearLayout) findViewById(R.id.lyt_add_tugas);
        lyt_from = (RelativeLayout) findViewById(R.id.from);
        lytDetailScrl = (ScrollView) findViewById(R.id.lyt_detail_tugas_scroll);
        lytEditScrl = (ScrollView) findViewById(R.id.lyt_add_tugas_scroll);

        mDateTextDetail = (TextView) findViewById(R.id.detail_set_date);
        mDateDeadlineTextDetail = (TextView) findViewById(R.id.detail_set_date_deadline);
        mTimeTextDetail = (TextView) findViewById(R.id.detail_set_time);
        mTimeDeadlineTextDetail = (TextView) findViewById(R.id.detail_set_time_deadline);

        lytDetail.setVisibility(View.VISIBLE);
        lytEdit.setVisibility(View.GONE);
        lytDetailScrl.setVisibility(View.VISIBLE);
        lytEditScrl.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_activity_detail_reminder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        pm = new PrefManager(this);
        mode = pm.getMode();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        mTitleText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTitle = s.toString().trim();
                mTitleText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mDescText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDesc = s.toString().trim();
                mDescText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        action = getIntent().getIntExtra("action", 0);

        mReceivedID = Integer.parseInt(getIntent().getStringExtra(EXTRA_TUGAS_ID));

        tb = new TugasDatabase(this);

        if (mode.equals("online")) {
            checkConnection();
            HashMap<String, String> user = tb.getUserDetails();
            member_id = user.get("uid");
            username = user.get("username");
            name = user.get("name");
        }

        if (action == 1) {
            mReceivedTugas = tb.getTugas(mReceivedID, 0);
        } else if (action == 2){
            lyt_from.setVisibility(View.VISIBLE);
            mReceivedTugas = tb.getTugasShared(0, mReceivedID);

        } else if (action == 3) {
            lyt_from.setVisibility(View.VISIBLE);

            int tugas_id = getIntent().getIntExtra("tugas_id", 0);
            String from = getIntent().getStringExtra("from");
            String title = getIntent().getStringExtra("title");
            String desc = getIntent().getStringExtra("desc");
            String date = getIntent().getStringExtra("date");
            String date_deadline = getIntent().getStringExtra("date_deadline");
            String time = getIntent().getStringExtra("time");
            String time_deadline = getIntent().getStringExtra("time_deadline");

            mReceivedID = tugas_id;

            mReceivedTugas = new Tugas(tugas_id, from, title, desc, date, date_deadline, time, time_deadline);
        } else if (action == 4 || action == 5) {
            lyt_from.setVisibility(View.VISIBLE);
            getTugasShared(member_id, String.valueOf(mReceivedID));
        } else if (action == 6) {
            lyt_from.setVisibility(View.VISIBLE);

            int tugas_id = getIntent().getIntExtra("tugas_id", 0);
            String from = getIntent().getStringExtra("from");
            String title = getIntent().getStringExtra("title");
            String desc = getIntent().getStringExtra("desc");
            String date = getIntent().getStringExtra("date");
            String date_deadline = getIntent().getStringExtra("date_deadline");
            String time = getIntent().getStringExtra("time");
            String time_deadline = getIntent().getStringExtra("time_deadline");

            mReceivedID = tugas_id;

            mReceivedTugas = new Tugas(tugas_id, from, title, desc, date, date_deadline, time, time_deadline);
        }

        if (action != 4 && action != 5) {
            mTugasID = mReceivedTugas.getReminderID();
            mTitle = mReceivedTugas.getTitle();
            mDesc = mReceivedTugas.getDesc();
            mDate = mReceivedTugas.getDate();
            mDateDeadline = mReceivedTugas.getDateDeadline();
            mTime = mReceivedTugas.getTime();
            mTimeDeadline = mReceivedTugas.getTimeDeadline();
            mFrom = mReceivedTugas.getFrom();


            mTitleText.setText(mTitle);
            lblTitle.setText(mTitle);
            mDescText.setText(mDesc);
            lblDesc.setText(mDesc);
            mDateText.setText(mDate);
            mDateDeadlineText.setText(mDateDeadline);
            mTimeText.setText(mTime);
            mTimeDeadlineText.setText(mTimeDeadline);
            mFromText.setText(mFrom);

            mDateTextDetail.setText(mDate);
            mDateDeadlineTextDetail.setText(mDateDeadline);
            mTimeTextDetail.setText(mTime);
            mTimeDeadlineTextDetail.setText(mTimeDeadline);

            if (savedInstanceState != null) {
                String savedTitle = savedInstanceState.getString(KEY_TITLE);
                mTitleText.setText(savedTitle);
                mTitle = savedTitle;

                String savedDesc = savedInstanceState.getString(KEY_DESC);
                mDescText.setText(savedDesc);
                mDesc = savedDesc;

                String savedTime = savedInstanceState.getString(KEY_TIME);
                mTimeText.setText(savedTime);
                mTime = savedTime;

                String savedDate = savedInstanceState.getString(KEY_DATE);
                mDateText.setText(savedDate);
                mDate = savedDate;

                String savedTimeDeadline = savedInstanceState.getString(KEY_TIME_DEADLINE);
                mTimeDeadlineText.setText(savedTimeDeadline);
                mTimeDeadline = savedTimeDeadline;

                String savedDateDeadline = savedInstanceState.getString(KEY_DATE_DEADLINE);
                mDateDeadlineText.setText(savedDateDeadline);
                mDateDeadline = savedDateDeadline;
            }

            mCalendar = Calendar.getInstance();
            //mAlarmReceiver = new AlarmReceiver();

            mDateSplit = mDate.split("/");
            mTimeSplit = mTime.split(":");

            mDay = Integer.parseInt(mDateSplit[0]);
            mMonth = Integer.parseInt(mDateSplit[1]);
            mYear = Integer.parseInt(mDateSplit[2]);
            mHour = Integer.parseInt(mTimeSplit[0]);
            mMinute = Integer.parseInt(mTimeSplit[1]);

            mDateDeadlineSplit = mDateDeadline.split("/");
            mTimeDeadlineSplit = mTimeDeadline.split(":");

            mDay2 = Integer.parseInt(mDateDeadlineSplit[0]);
            mMonth2 = Integer.parseInt(mDateDeadlineSplit[1]);
            mYear2 = Integer.parseInt(mDateDeadlineSplit[2]);
            mHour2 = Integer.parseInt(mTimeDeadlineSplit[0]);
            mMinute2 = Integer.parseInt(mTimeDeadlineSplit[1]);
        }
    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(KEY_TITLE, mTitleText.getText());
        outState.putCharSequence(KEY_DESC, mDescText.getText());
        outState.putCharSequence(KEY_TIME, mTimeText.getText());
        outState.putCharSequence(KEY_TIME_DEADLINE, mTimeDeadlineText.getText());
        outState.putCharSequence(KEY_DATE, mDateText.getText());
        outState.putCharSequence(KEY_DATE_DEADLINE, mDateDeadlineText.getText());
    }

    public void setTime(View v){
        stat = 0;
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void setTime2(View v){
        stat = 1;
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void setDate(View v){
        stat = 0;
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void setDate2(View v){
        stat = 1;
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        mHour = hourOfDay;
        mMinute = minute;
        mHour2 = hourOfDay;
        mMinute2 = minute;
        if (stat == 0) {
            if (minute < 10) {
                mTime = hourOfDay + ":" + "0" + minute;
            } else {
                mTime = hourOfDay + ":" + minute;
            }
            mTimeText.setText(mTime);
        } else {
            if (minute < 10) {
                mTimeDeadline = hourOfDay + ":" + "0" + minute;
            } else {
                mTimeDeadline = hourOfDay + ":" + minute;
            }
            mTimeDeadlineText.setText(mTimeDeadline);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear ++;
        if(stat == 0) {
            mDay = dayOfMonth;
            mMonth = monthOfYear;
            mYear = year;
            mDate = dayOfMonth + "/" + monthOfYear + "/" + year;
            mDateText.setText(mDate);
        } else {
            mDay2 = dayOfMonth;
            mMonth2 = monthOfYear;
            mYear2 = year;
            mDateDeadline = dayOfMonth + "/" + monthOfYear + "/" + year;
            mDateDeadlineText.setText(mDateDeadline);
        }
    }

    public void updateReminder() {
        mReceivedTugas.setReminderID(mTugasID);
        mReceivedTugas.setTitle(mTitle);
        mReceivedTugas.setDesc(mDesc);
        mReceivedTugas.setDate(mDate);
        mReceivedTugas.setDateDeadline(mDateDeadline);
        mReceivedTugas.setTime(mTime);
        mReceivedTugas.setTimeDeadline(mTimeDeadline);

        tb.updateTugas(mReceivedTugas);
        if (mode.equals("online")) {
            boolean isConnected = ConnectivityReceiver.isConnected();
            if (isConnected) {
                updateReminder(mReceivedTugas, member_id);
            }
        } else if (mode.equals("offline")) {
            Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        mCalendar.set(Calendar.MONTH, --mMonth);
        mCalendar.set(Calendar.YEAR, mYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, mDay);
        mCalendar.set(Calendar.HOUR_OF_DAY, mHour);
        mCalendar.set(Calendar.MINUTE, mMinute);
        mCalendar.set(Calendar.SECOND, 0);
    }

    @Override
    public void onBackPressed() {

        if (saved) {
            Intent i = new Intent();
            setResult(RESULT_OK, i);
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_reminder, menu);
        final MenuItem save = menu.findItem(R.id.save_reminder);
        MenuItem discard = menu.findItem(R.id.discard_reminder);
        final MenuItem edit = menu.findItem(R.id.edit_reminder);
        MenuItem download = menu.findItem(R.id.download);
        MenuItem sync = menu.findItem(R.id.sync);

        if (action == 1) {
            save.setVisible(false);
            edit.setVisible(true);
        } else if (action == 2){
            edit.setVisible(false);
            save.setVisible(false);
            discard.setVisible(false);
        } else if (action == 3 || action == 4) {
            download.setVisible(true);
            edit.setVisible(false);
            save.setVisible(false);
            discard.setVisible(false);
        } else if (action == 5 || action == 6) {
            sync.setVisible(true);
            save.setVisible(false);
            discard.setVisible(false);
        }

        edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                save.setVisible(true);
                edit.setVisible(false);
                return false;
            }
        });

        download.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                saveToLocal(member_id, String.valueOf(mReceivedID));
                return false;
            }
        });

        sync.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                syncTugas(member_id, String.valueOf(mReceivedID));
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.edit_reminder:
                lytDetail.setVisibility(View.GONE);
                lytEdit.setVisibility(View.VISIBLE);
                lytDetailScrl.setVisibility(View.GONE);
                lytEditScrl.setVisibility(View.VISIBLE);

                getSupportActionBar().setTitle(R.string.title_activity_edit_reminder);
                return true;

            case R.id.save_reminder:
                mTitleText.setText(mTitle);

                if (mTitleText.getText().toString().length() == 0) {
                    mTitleText.setError("Judul Tidak Boleh Kosong!");
                } else if (mDescText.getText().toString().length() == 0) {
                    mDescText.setError("Deskripsi Tidak Boleh Kosong");
                } else {
                    boolean isConnected = ConnectivityReceiver.isConnected();
                    if (!isConnected) {
                        showSnack(isConnected);
                    }
                    updateReminder();
                }
                return true;

            case R.id.discard_reminder:
                Toast.makeText(getApplicationContext(), "Batal",
                        Toast.LENGTH_SHORT).show();

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.time_deadline), message, Snackbar.LENGTH_INDEFINITE).setAction("MODE OFFLINE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tb.deleteUsers();
                    pm.setMode("offline");
                    mode = "offline";
                    Toast.makeText(getApplicationContext(), "MODE OFFLINE AKTIF", Toast.LENGTH_SHORT).show();
                }
            });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    private void updateReminder(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Menyimpan ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi tidak stabil", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", String.valueOf(tugas.getReminderID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("member_id", member_id);
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveToLocal(final String member_id, final String tugas_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE_TO_LOCAL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if (!error) {
                        JSONObject reminder = jObj.getJSONObject("reminder");

                        int reminder_id = reminder.getInt("reminder_id");
                        if (reminder_id != 0) {

                            if (action == 3) {
                                saved = true;

                                int tugas_id = mReceivedTugas.getReminderID();
                                String from = mReceivedTugas.getFrom();
                                String title = mReceivedTugas.getTitle();
                                String desc = mReceivedTugas.getDesc();
                                String date = mReceivedTugas.getDate();
                                String date_deadline = mReceivedTugas.getDateDeadline();
                                String time = mReceivedTugas.getTime();
                                String time_deadline = mReceivedTugas.getTimeDeadline();

                                tb.addSharedTugas(new Tugas(tugas_id, from, title, desc, date, date_deadline, time, time_deadline));
                            } else if (action == 4) {
                                String from = mFromText.getText().toString();
                                String title = lblTitle.getText().toString();
                                String desc = lblDesc.getText().toString();
                                String date = mDateTextDetail.getText().toString();
                                String date_deadline = mDateDeadlineTextDetail.getText().toString();
                                String time = mTimeTextDetail.getText().toString();
                                String time_deadline = mTimeDeadlineTextDetail.getText().toString();

                                tb.addSharedTugas(new Tugas(mReceivedID, from, title, desc, date, date_deadline, time, time_deadline));
                            }

                            Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
                        }
                        onResume();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "SaveToLocal Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);
                params.put("tugas_id", tugas_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getTugasShared(final String member_id, final String tugas_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_TUGAS_SHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    /*int a = reminder_list.length() - test.size();
                    selisih.add(String.valueOf(a));
                    Toast.makeText(getApplicationContext(), selisih.get(0), Toast.LENGTH_LONG).show();*/

                    //if (test.size() < reminder_list.length()) {
                    // Check for error node in json
                    if (!error) {
                        JSONObject reminder = jObj.getJSONObject("tugas");

                        int tugas_id = reminder.getInt("tugas_id");
                        String from = reminder.getString("name");
                        String title = reminder.getString("title");
                        String desc = reminder.getString("desc");
                        String date = reminder.getString("date");
                        String date_deadline = reminder.getString("date_deadline");
                        String time = reminder.getString("time");
                        String time_deadline = reminder.getString("time_deadline");

                        lblTitle.setText(title);
                        lblDesc.setText(desc);
                        mDateTextDetail.setText(date);
                        mDateDeadlineTextDetail.setText(date_deadline);
                        mTimeTextDetail.setText(time);
                        mTimeDeadlineTextDetail.setText(time_deadline);
                        mFromText.setText(from);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "getTugasShared Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);
                params.put("tugas_id", tugas_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void syncTugas(final String member_id, final String tugas_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_TUGAS_SHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if (!error) {
                        JSONObject reminder = jObj.getJSONObject("reminder");

                        int reminder_id = reminder.getInt("reminder_id");
                        if (reminder_id != 0) {
                            if (action == 6) {
                                saved = true;

                                int tugas_id = mReceivedTugas.getReminderID();
                                String from = mReceivedTugas.getFrom();
                                String title = mReceivedTugas.getTitle();
                                String desc = mReceivedTugas.getDesc();
                                String date = mReceivedTugas.getDate();
                                String date_deadline = mReceivedTugas.getDateDeadline();
                                String time = mReceivedTugas.getTime();
                                String time_deadline = mReceivedTugas.getTimeDeadline();

                                tb.updateTugasShared(new Tugas(tugas_id, from, title, desc, date, date_deadline, time, time_deadline));
                            } else if (action == 5) {
                                String from = mFromText.getText().toString();
                                String title = lblTitle.getText().toString();
                                String desc = lblDesc.getText().toString();
                                String date = mDateTextDetail.getText().toString();
                                String date_deadline = mDateDeadlineTextDetail.getText().toString();
                                String time = mTimeTextDetail.getText().toString();
                                String time_deadline = mTimeDeadlineTextDetail.getText().toString();

                                tb.updateTugasShared(new Tugas(mReceivedID, from, title, desc, date, date_deadline, time, time_deadline));
                            }

                            Toast.makeText(getApplicationContext(), "Diperbaharui", Toast.LENGTH_SHORT).show();
                        }
                        onResume();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Sync Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);
                params.put("tugas_id", tugas_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
