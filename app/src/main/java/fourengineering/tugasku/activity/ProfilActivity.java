package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.TugasDatabase;

public class ProfilActivity extends AppCompatActivity {

    private static final String TAG = ProfilActivity.class.getSimpleName();
    private TugasDatabase tb;
    private Toolbar mToolbar;
    private TextView lblNama, lblDisplayName, lblEmail, lblDisplayEmail, lblUsername, lblUID, lblCreated, lblTlp;
    private String nama, email, username, uid, created, tlp;
    private ProgressDialog pDialog;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;
    private ImageView profilImage;
    private RelativeLayout lyt_no_tlp, lyt_nama, lyt_change_pass, lyt_email;
    private String currentPassword;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        user = FirebaseAuth.getInstance().getCurrentUser();

        lblNama = (TextView) findViewById(R.id.labelName);
        lblDisplayName = (TextView) findViewById(R.id.lblName);
        lblDisplayEmail = (TextView) findViewById(R.id.email);
        lblEmail = (TextView) findViewById(R.id.lblEmail);
        lblUsername = (TextView) findViewById(R.id.lblUsername);
        lblUID = (TextView) findViewById(R.id.lblUserID);
        lblCreated = (TextView) findViewById(R.id.lblCreated);
        lblTlp = (TextView) findViewById(R.id.lblTlp);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        profilImage = (ImageView) findViewById(R.id.profile_image);
        lyt_nama = (RelativeLayout) findViewById(R.id.lyt_nama);
        lyt_no_tlp = (RelativeLayout) findViewById(R.id.lyt_no_tlp);
        lyt_change_pass = (RelativeLayout) findViewById(R.id.lyt_change_pass);
        lyt_email = (RelativeLayout) findViewById(R.id.lyt_email);
        tb = new TugasDatabase(getApplicationContext());

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.profil_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Intent i = getIntent();
        String username = i.getStringExtra("username");

        if(username == null) {
            getUserInfo();
        } else {
            uid = i.getStringExtra("member_id");
            nama = i.getStringExtra("name");
            email = i.getStringExtra("email");
            tlp = i.getStringExtra("notlp");
            created = i.getStringExtra("created_at");

            lblNama.setText(nama + "");
            lblDisplayName.setText(nama + "");
            lblEmail.setText(email + "");
            lblDisplayEmail.setText(email + "");
            lblUID.setText(uid + "");
            lblUsername.setText(username + "");
            lblCreated.setText(created + "");
            if (tlp.isEmpty()) {
                lblTlp.setText("Tidak Tersedia");
            } else {
                lblTlp.setText(tlp + "");
            }

            lyt_nama.setClickable(false);
            lyt_no_tlp.setClickable(false);
            lyt_email.setClickable(false);
            lyt_change_pass.setVisibility(View.GONE);
        }

        String letter = "A";

        if(nama != null && !nama.isEmpty()) {
            letter = nama.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();
        mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
        profilImage.setImageDrawable(mDrawableBuilder);
    }

    public void getUserInfo() {
        HashMap<String, String> user = tb.getUserDetails();

        username = user.get("username");
        nama = user.get("name");
        email = user.get("email");
        currentPassword = user.get("password");
        tlp = user.get("no_tlp");
        uid = user.get("uid");
        created = user.get("created_at");

        lblNama.setText(nama + "");
        lblDisplayName.setText(nama + "");
        lblEmail.setText(email + "");
        lblDisplayEmail.setText(email + "");
        lblUID.setText(uid + "");
        lblUsername.setText(username + "");
        lblCreated.setText(created + "");
        if (tlp == null) {
            lblTlp.setText("Tidak Tersedia");
        } else {
            lblTlp.setText(tlp + "");
        }
    }

    public void setTlp(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_tlp, null);
        alert.setIcon(R.drawable.ic_tlp_white);
        alert.setTitle("No Telepon");
        alert.setView(dialogView);
        final EditText input = (EditText) dialogView.findViewById(R.id.ch_tlp);
        String notlp = lblTlp.getText().toString();
        if (!notlp.isEmpty()) {
            input.setText(notlp);
        }
        alert.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String noTlp = input.getText().toString();
                if (!noTlp.isEmpty()) {
                    updateMember(username, nama, email, noTlp);
                    lblTlp.setText(noTlp);
                } else {
                    input.setError("Masukkan No Telepon");
                }
            }
        });
        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void setName(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_name, null);
        alert.setTitle("Nama Lengkap");
        alert.setView(dialogView);
        final EditText input = (EditText) dialogView.findViewById(R.id.ch_name);
        String name = lblDisplayName.getText().toString();
        if (!name.isEmpty()) {
            input.setText(name);
        }
        alert.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                if (!name.isEmpty()) {
                    UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name).build();
                    user.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d(TAG, "User profile updated.");
                        }
                    });
                    updateMember(username, name, email, tlp);
                    lblDisplayName.setText(name);
                    lblNama.setText(name);

                    String namaDraw = lblDisplayName.getText().toString();

                    String letter = "A";

                    if(namaDraw != null && !namaDraw.isEmpty()) {
                        letter = namaDraw.substring(0, 1);
                    }

                    int color = mColorGenerator.getRandomColor();
                    mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
                    profilImage.setImageDrawable(mDrawableBuilder);
                } else {
                    input.setError("Masukkan No Telepon");
                }
            }
        });
        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void setEmail(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_email, null);
        alert.setIcon(R.drawable.ic_email_white_24dp);
        alert.setTitle("Email");
        alert.setView(dialogView);
        final EditText input = (EditText) dialogView.findViewById(R.id.ch_email);
        String email = lblEmail.getText().toString();
        if (!email.isEmpty()) {
            input.setText(email);
        }
        alert.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = input.getText().toString();
                if (!email.isEmpty()) {
                    updateMember(username, nama, email, tlp);
                    lblEmail.setText(email);
                    lblDisplayEmail.setText(email);
                } else {
                    input.setError("Masukkan No Telepon");
                }
            }
        });
        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void changePassword(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_password, null);
        alert.setIcon(R.drawable.ic_pass_white_24dp);
        alert.setTitle("Ganti Password");
        alert.setView(dialogView);
        final EditText passLama = (EditText) dialogView.findViewById(R.id.passLama);
        final EditText passBaru = (EditText) dialogView.findViewById(R.id.passBaru);
        final EditText konfPass = (EditText) dialogView.findViewById(R.id.konfPass);
        final CheckBox lihat_pwd = (CheckBox) dialogView.findViewById(R.id.cb_lihat_pwd);

        lihat_pwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    passLama.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passBaru.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    konfPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    passLama.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passBaru.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    konfPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        alert.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordLama = passLama.getText().toString();
                final String passwordBaru = passBaru.getText().toString();
                String konfPwd = konfPass.getText().toString();

                if ((!passwordLama.isEmpty()) && (!passwordBaru.isEmpty()) && (!konfPwd.isEmpty())) {
                    if (passwordBaru.equals(konfPwd)) {
                        if (passwordLama.equals(currentPassword)) {
                            //changePassword(username, passwordLama, passwordBaru);
                            user.updatePassword(passwordBaru).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        tb.updatePassword(username, passwordBaru);
                                        currentPassword = passwordBaru;
                                        Toast.makeText(getApplicationContext(), "Password Berhasil Dirubah", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Password Gagal Dirubah", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "Password Lama Salah!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        TextView lblInfo = new EditText(getApplicationContext());
                        lblInfo.setText(R.string.error_change_pwd);
                        lblInfo.setGravity(Gravity.CENTER_VERTICAL);
                        String info = lblInfo.getText().toString();
                        Toast.makeText(getApplicationContext(), info, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Field Tidak Boleh Ada Yang Kosng", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateMember(final String username, final String name, final String email, final String noTlp) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memperbaharui ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEMEMBER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("member");
                        String username = user.getString("username");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String notlp = user.getString("no_tlp");

                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();

                        // Inserting row in users table
                        tb.updateMember(username, name, email, notlp);

                        // Launch main activity
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("name", name);
                params.put("email", email);
                params.put("no_tlp", noTlp);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void changePassword(final String username, final String passwordLama, final String passwordBaru) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memperbaharui ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CHANGEPASSWORD, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        String success = jObj.getString("success_msg");

                        Toast.makeText(getApplicationContext(), success, Toast.LENGTH_LONG).show();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password_lama", passwordLama);
                params.put("password_baru", passwordBaru);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
