package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.TemanReq;
import fourengineering.tugasku.adapter.TemanReqAdapter;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.TugasDatabase;

public class TemanReqListActivity extends AppCompatActivity {
    private static final String TAG = TemanReqListActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private TextView lblNoFriendReq;
    private ImageView imgNoFriendReq;
    private SwipeMenuListView mListReq;
    private ProgressDialog pDialog;
    private TugasDatabase tb;
    private String member_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teman_req_list);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mListReq = (SwipeMenuListView) findViewById(R.id.req_list_teman);
        lblNoFriendReq = (TextView) findViewById(R.id.no_friends_text);
        imgNoFriendReq = (ImageView) findViewById(R.id.ic_friends_req);


        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.req_teman_text);

        tb = new TugasDatabase(getApplicationContext());
        HashMap<String, String> user = tb.getUserDetails();
        member_id = user.get("uid");
        //getFriendListReq(member_id);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem accReq = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                accReq.setBackground(R.color.green_800);
                // set item width
                accReq.setWidth((int) getResources().getDimension(R.dimen._55sdp));
                // set a icon
                accReq.setIcon(R.drawable.ic_check_white_24dp);
                // add to menu
                menu.addMenuItem(accReq);

                SwipeMenuItem decline = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                decline.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                decline.setWidth((int) getResources().getDimension(R.dimen._55sdp));
                // set a icon
                decline.setIcon(R.drawable.ic_delete_white_24dp);
                // add to menu
                menu.addMenuItem(decline);
            }
        };

        mListReq.setMenuCreator(creator);

        mListReq.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                TemanReq temanReq = (TemanReq) mListReq.getAdapter().getItem(position);
                String member_id_me = member_id;
                String member_id_req = temanReq.member_id.toString();
                String status = "";
                switch (index) {
                    case 0:
                        status = "1";
                        AcceptOrDecline(member_id_req, member_id_me, status);
                        break;
                    case 1:
                        status = "2";
                        AcceptOrDecline(member_id_req, member_id_me, status);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFriendListReq(member_id);
    }

    private void getFriendListReq(final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat ...");
        showDialog();

        ArrayList<TemanReq> arrayReqTeman = new ArrayList<TemanReq>();
        final TemanReqAdapter temanReqAdapter = new TemanReqAdapter(getBaseContext(), arrayReqTeman);
        mListReq.setAdapter(temanReqAdapter);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REQFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");
                        //JSONArray teman = null;
                        JSONArray teman = jObj.getJSONArray("req_teman");

                        for (int i=0; i<teman.length(); i++) {
                            JSONObject c = teman.getJSONObject(i);
                            String member_id = c.getString("member_id");
                            String name = c.getString("name");
                            String email = c.getString("email");

                            TemanReq _teman = new TemanReq(member_id, name, email);
                            temanReqAdapter.add(_teman);
                        }

                        // Inserting row in users table
                        //rb.addUser(username, name, email, uid, created_at);

                        // Launch main activity
                    } else {
                        lblNoFriendReq.setVisibility(View.VISIBLE);
                        imgNoFriendReq.setVisibility(View.VISIBLE);
                        String errorMsg = jObj.getString("error_msg");
                        lblNoFriendReq.setText(errorMsg);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void AcceptOrDecline(final String member_id_req, final String member_id_me, final String status) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat ...");
        //showDialog();

        ArrayList<TemanReq> arrayReqTeman = new ArrayList<TemanReq>();
        final TemanReqAdapter temanReqAdapter = new TemanReqAdapter(getBaseContext(), arrayReqTeman);
        mListReq.setAdapter(temanReqAdapter);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACCORDC, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        String status = jObj.getString("status");
                        if (status == "1") {
                            Toast.makeText(getApplicationContext(), "Diterima", Toast.LENGTH_SHORT).show();
                        } else if (status == "2") {
                            Toast.makeText(getApplicationContext(), "Ditolak", Toast.LENGTH_SHORT).show();
                        }
                        onResume();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id_req", member_id_req);
                params.put("member_id_me", member_id_me);
                params.put("status", status);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
