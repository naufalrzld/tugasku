package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Teman;
import fourengineering.tugasku.adapter.TemanAdapter;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.TugasDatabase;

public class ListTemanActivity extends AppCompatActivity {
    private static final String TAG = ListTemanActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private SwipeMenuListView mListTeman;
    private ProgressDialog pDialog;
    private TugasDatabase tb;
    private String member_id, username, nama;
    private FloatingActionButton mShare, mAddFriend;
    private LinearLayout LytReq;
    private TextView lblReq, lblNoFriends;
    private ImageView imgNoFriends;

    private static final String TAG_TEMANKU = "temanku";
    private static final String TAG_NAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MEMBERID = "member_id";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_FRIENDLIST_ID = "friend_list_id";

    ArrayList<HashMap<String, String>> listTeman;

    boolean checkbox = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_teman);

        LytReq = (LinearLayout) findViewById(R.id.req_friend);
        lblReq = (TextView) findViewById(R.id.lblReq);
        mShare = (FloatingActionButton) findViewById(R.id.fabShare);
        mAddFriend = (FloatingActionButton) findViewById(R.id.fabAdd);
        lblNoFriends = (TextView) findViewById(R.id.no_friends_text);
        imgNoFriends = (ImageView) findViewById(R.id.ic_friends);

        Intent i = getIntent();

        final ArrayList<Integer> id_reminder = i.getIntegerArrayListExtra("ID");
        if (id_reminder != null) {
            checkbox = true;
            mShare.setVisibility(View.VISIBLE);
            mAddFriend.setVisibility(View.GONE);
        }

        listTeman = new ArrayList<HashMap<String, String>>();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mListTeman = (SwipeMenuListView) findViewById(R.id.list_teman);

        tb = new TugasDatabase(getApplicationContext());
        HashMap<String, String> user = tb.getUserDetails();
        member_id = user.get("uid");
        username = user.get("username");
        nama = user.get("name");

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth((int) getResources().getDimension(R.dimen._55sdp));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_trash_white_24dp);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        mListTeman.setMenuCreator(creator);

        mListTeman.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        if (id_reminder == null) {
                            Teman teman = (Teman) mListTeman.getAdapter().getItem(position);
                            String member_id_one = member_id;
                            String member_id_two = teman.member_id.toString();
                            deleteFriend(member_id_one, member_id_two);
                        }
                        break;
                }
                return false;
            }
        });

        mListTeman.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id_reminder == null) {
                    Teman teman = (Teman) mListTeman.getAdapter().getItem(position);
                    getFriendDetil(teman.member_id);
                }

                CheckBox cb = (CheckBox) view.findViewById(R.id.cbTeman);
                cb.setChecked(!cb.isChecked());
            }
        });

        mListTeman.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //getFriendList(member_id);

        mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = mListTeman.getCheckedItemPositions();

                for (int i = 0; i < checked.size(); i++) {
                    Teman teman = (Teman) mListTeman.getAdapter().getItem(checked.keyAt(i));
                    shareReminder(member_id, teman.member_id, id_reminder.toString(), teman.friend_list_id.toString(), teman.username);
                    sendNotif(teman.username, "Tugas", "Tuags Baru Dari " + nama, id_reminder.toString());
                }
            }
        });

        mAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListTemanActivity.this, CariTemanActivity.class);
                startActivity(i);
            }
        });

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.teman_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //getFriendReqCount(member_id);
    }

    public void FriendReq(View v) {
        Intent i = new Intent(ListTemanActivity.this, TemanReqListActivity.class);
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFriendList(member_id);
        getFriendReqCount(member_id);

        if (mListTeman != null) {
            imgNoFriends.setVisibility(View.GONE);
            lblNoFriends.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_friend_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
			/*case R.id.tambah_teman:
				tambahTeman();
				return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getFriendReqCount(final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETFRIENDREQCOUNT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        String friend_req = jObj.getString("friend_req");
                        LytReq.setVisibility(View.VISIBLE);
                        lblReq.setText(friend_req);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        LytReq.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getFriendList(final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data ...");
        showDialog();


        ArrayList<Teman> arrayTeman = new ArrayList<Teman>();
        final TemanAdapter temanAdapter = new TemanAdapter(getBaseContext(), arrayTeman, checkbox);
        mListTeman.setAdapter(temanAdapter);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETFRIENDLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");
                        //JSONArray teman = null;
                        JSONArray teman = jObj.getJSONArray(TAG_TEMANKU);

                        for (int i=0; i<teman.length(); i++) {
                            JSONObject c = teman.getJSONObject(i);
                            String friend_list_id = c.getString(TAG_FRIENDLIST_ID);
                            String member_id = c.getString(TAG_MEMBERID);
                            String username = c.getString(TAG_USERNAME);
                            String name = c.getString(TAG_NAME);
                            String email = c.getString(TAG_EMAIL);

                            Teman _teman = new Teman(friend_list_id, member_id, username, name, email);
                            temanAdapter.add(_teman);
                        }

                        // Inserting row in users table
                        //rb.addUser(username, name, email, uid, created_at);

                        // Launch main activity
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        imgNoFriends.setVisibility(View.VISIBLE);
                        lblNoFriends.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getFriendDetil(final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data ...");
        showDialog();

		/*ArrayList<Teman> arrayTeman = new ArrayList<Teman>();
		final TemanAdapter temanAdapter = new TemanAdapter(getBaseContext(), arrayTeman);
		mListTeman.setAdapter(temanAdapter);*/

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETDETILFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // Now store the user in SQLite
                        String uid = jObj.getString("member_id");

                        JSONObject user = jObj.getJSONObject("friend");
                        String username = user.getString("username");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String notlp = user.getString("no_tlp");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table

                        // Launch main activity
                        Intent intent = new Intent(ListTemanActivity.this, ProfilActivity.class);
                        intent.putExtra("member_id", uid);
                        intent.putExtra("username", username);
                        intent.putExtra("name", name);
                        intent.putExtra("email", email);
                        intent.putExtra("notlp", notlp);
                        intent.putExtra("created_at", created_at);

                        startActivity(intent);
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id", member_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void shareReminder(final String pengirim, final String penerima, final String reminder_id, final String friend_list_id, final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mengirim ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SHARE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("share");
                        //String username = user.getString("username");
                        String pengirim = user.getString("pengirim");

                        Toast.makeText(getApplicationContext(), "Share Success", Toast.LENGTH_LONG).show();

                        onBackPressed();

                        // Inserting row in users table
                        //rb.addUser(username, name, email, uid, created_at);

                        // Launch main activity
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("pengirim", pengirim);
                params.put("penerima", penerima);
                params.put("reminder_id", reminder_id);
                params.put("friend_list_id", friend_list_id);
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void sendNotif(final String username, final String title, final String message, final String tugas_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SEND_SINGLE_PUSH, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Log.d(TAG, "Push Success");
                    } else {
                        Log.d(TAG, "Push Not Success");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("title", title);
                params.put("message", message);
                params.put("tugas_id", tugas_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void deleteFriend(final String member_id_one, final String member_id_two) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        String successmsg = jObj.getString("success_msg");
                        Toast.makeText(getApplicationContext(), successmsg, Toast.LENGTH_LONG).show();
                        onResume();

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id_one", member_id_one);
                params.put("member_id_two", member_id_two);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
