package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.app.MyNotificationManager;
import fourengineering.tugasku.helper.TugasDatabase;

public class CariTemanActivity extends AppCompatActivity {
    private static final String TAG = CariTemanActivity.class.getSimpleName();
    Toolbar mToolbar;
    TextView lblNama, lblUsername, lblInfo, lblAddSuccess;
    EditText txtUsername;
    ImageView imgProfil;
    Button btnAdd;
    ImageButton btnSearch;
    private String username, usernameme, member_id_one, nama;
    RelativeLayout rl;
    private ProgressDialog pDialog;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;
    private TugasDatabase tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_teman);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        lblNama = (TextView) findViewById(R.id.labelName);
        lblUsername = (TextView) findViewById(R.id.username);
        lblInfo = (TextView) findViewById(R.id.lblInfo);
        lblAddSuccess = (TextView) findViewById(R.id.lblAddSuccess);
        imgProfil = (ImageView) findViewById(R.id.profile_image);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        rl = (RelativeLayout) findViewById(R.id.content);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.search_teman);

        tb = new TugasDatabase(getApplicationContext());
        HashMap<String, String> user = tb.getUserDetails();
        member_id_one = user.get("uid");
        usernameme = user.get("username");
        nama = user.get("name");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = txtUsername.getText().toString();
                if (txtUsername.getText().toString().length() == 0){
                    txtUsername.setError("Masukkan Username!");
                }
                else if (username.equals(usernameme)){
                    lblInfo.setVisibility(View.VISIBLE);
                    rl.setVisibility(View.GONE);
                    lblInfo.setText("Username Milik Anda!");
                } else {
                    checkUsername(username, usernameme);
                }
            }
        });
    }

    private void checkUsername(final String username, final String usernameme) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETUNAME, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        final String member_id_two = jObj.getString("member_id");

                        JSONObject user = jObj.getJSONObject("friend");
                        //String username = user.getString("username");
                        final String username = user.getString("username");
                        final String name = user.getString("name");
                        //String created_at = user.getString("created_at");

                        String letter = "A";

                        if(name != null && !name.isEmpty()) {
                            letter = name.substring(0, 1);
                        }

                        int color = mColorGenerator.getRandomColor();
                        mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
                        imgProfil.setImageDrawable(mDrawableBuilder);

                        rl.setVisibility(View.VISIBLE);
                        lblInfo.setVisibility(View.GONE);
                        lblNama.setText(name);
                        lblUsername.setText(username);

                        btnAdd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new MyNotificationManager(getApplicationContext());
                                tambahTeman(member_id_one, member_id_two, username);
                                //sendNotif(username, "Teman", "Permintaan Pertemanan Dari " + nama);
                            }
                        });

                        // Inserting row in users table
                        //rb.addUser(username, name, email, uid, created_at);

                        // Launch main activity
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(),errorMsg, Toast.LENGTH_LONG).show();
                        lblInfo.setVisibility(View.VISIBLE);
                        rl.setVisibility(View.GONE);
                        lblInfo.setText(errorMsg);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("usernameme", usernameme);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void tambahTeman(final String member_id_one, final String member_id_two, final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Menambahkan Teman ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDFRIEND, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // Now store the user in SQLite
                        //String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("addFriend");
                        //String username = user.getString("username");
                        String success = user.getString("success");

                        Toast.makeText(getApplicationContext(), success, Toast.LENGTH_SHORT).show();
                        btnAdd.setVisibility(View.GONE);
                        lblAddSuccess.setVisibility(View.VISIBLE);

                        // Inserting row in users table
                        //rb.addUser(username, name, email, uid, created_at);

                        // Launch main activity
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_id_one", member_id_one);
                params.put("member_id_two", member_id_two);
                params.put("action_user_id", member_id_one);
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void sendNotif(final String username, final String title, final String message) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SEND_SINGLE_PUSH, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Log.d(TAG, "Push Success");
                    } else {
                        Log.d(TAG, "Push Not Success");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("title", title);
                params.put("message", message);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
