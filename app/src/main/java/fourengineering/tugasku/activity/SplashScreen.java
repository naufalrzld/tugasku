package fourengineering.tugasku.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import fourengineering.tugasku.R;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.SessionManager;

public class SplashScreen extends AppCompatActivity {

    private static int splashInterval = 2000;
    private SessionManager session;
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            setContentView(R.layout.activity_splash_screen);
            Thread myThread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(splashInterval);
                        session = new SessionManager(getApplicationContext());
                        if (!session.isLoggedIn()) {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.fade_out);
                            startActivity(i, options.toBundle());
                        } else {
                            Intent i = new Intent(getApplicationContext(), MainActivity2.class);
                            ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.fade_out);
                            i.putExtra("status", "OnSession");
                            startActivity(i, options.toBundle());
                        }
                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            myThread.start();
        } else {
            startActivity(new Intent(SplashScreen.this, WelcomeActivity.class));
            finish();
        }
    }
}
