package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.AlarmReceiver;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.DateTimeSorter;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.SessionManager;
import fourengineering.tugasku.helper.TugasDatabase;

public class MainActivity2 extends AppCompatActivity implements com.github.amlcurran.showcaseview.OnShowcaseEventListener,
        ConnectivityReceiver.ConnectivityReceiverListener {

    private RecyclerView mList;
    private TugasAdapter mAdapter;

    private SessionManager session;
    private Toolbar mToolbar;
    private SwipeRefreshLayout refreshLayout;
    private FloatingActionButton fabAdd;
    private TextView mNoReminderView, lblName, lblEmail;
    private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
    private TextDrawable mDrawableBuilder;
    private MultiSelector mMultiSelector = new MultiSelector();
    private int mTempPost;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private TugasDatabase tb;
    private PrefManager pm;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ImageView profilImage, mIcadd;

    private FirebaseAuth mAuth;

    private String name = null;
    private String email= null;
    private String member_id;
    private String username;
    private String mode;
    private int action;

    private ProgressDialog pDialog;
    private AlarmReceiver mAlarmReceiver;

    private boolean status_delete;

    private static final String TAG = MainActivity2.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);
        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);

        mList = (RecyclerView) findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) findViewById(R.id.no_reminder_text);
        mIcadd = (ImageView) findViewById(R.id.ic_add);
        lblName = (TextView) findViewById(R.id.drawerName);
        lblEmail = (TextView) findViewById(R.id.drawerEmail);
        profilImage = (ImageView) findViewById(R.id.profile_image);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        session = new SessionManager(getApplicationContext());

        pm = new PrefManager(this);
        mode = pm.getMode();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        mAuth = FirebaseAuth.getInstance();

        setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);

        tb = new TugasDatabase(getApplicationContext());

        if (mode.equals("offline")) {
            hideMenu();
            name = "MODE OFFLINE";
            email = "Anda sedang dalam mode ofline";
            refreshLayout.setEnabled(false);
        } else if (mode.equals("online")){
            checkConnection();
            HashMap<String, String> user = tb.getUserDetails();

            name = user.get("name");
            username = user.get("username");
            email = user.get("email");
            member_id = user.get("uid");

            if (getIntent().getStringExtra("status").equals("login")) {
                getReminderList(username);
                //syncTugas(username);
                getReminderListShared(username);
            }
        }

        /*List<Tugas> tugas = tb.getAllTugas();

        if (tugas.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        }*/

        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new TugasAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        mAlarmReceiver = new AlarmReceiver();

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddTugasKuActivity.class);
                startActivity(intent);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) {
                    drawerLayout.closeDrawers();
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(false);
                    drawerLayout.closeDrawers();
                }

                boolean connect = ConnectivityReceiver.isConnected();
                Intent i;
                switch (menuItem.getItemId()) {
                    case R.id.profile:
                        i = new Intent(MainActivity2.this, ProfilActivity.class);
                        i.putExtra("nama", name);
                        i.putExtra("email", email);
                        startActivity(i);
                        return true;
                    case R.id.list_teman:
                        i = new Intent(MainActivity2.this, ListTemanActivity.class);
                        startActivity(i);
                        return true;
                    case R.id.shared:
                        i = new Intent(MainActivity2.this, ShareTugasActivity.class);
                        startActivity(i);
                        return true;
                    case R.id.tentang:
                        showAbout();
                        return true;
                    case R.id.Setting:
                        startActivity(new Intent(MainActivity2.this, SettingActivity.class));
                        return true;
                    case R.id.offonmode:
                        tb.deleteUsers();
                        pm.setMode("offline");
                        startActivity(new Intent(MainActivity2.this, SplashScreen.class));
                        finish();
                        return true;
                    case R.id.onlineMode:
                        if (connect) {
                            logoutUser();
                        } else {
                            showSnack(connect);
                        }
                        return true;
                    case R.id.logout:
                        if (connect) {
                            HashMap<String, String> user = tb.getUserDetails();
                            String username = user.get("username");
                            resetToken(username);
                            tb.deleteUsers();
                            List<Tugas> mTest = tb.getAllTugas();
                            List<Tugas> mTest1 = tb.getAllTugasShared();
                            if (!mTest.isEmpty() || !mTest1.isEmpty()) {
                                tb.deleteAllTugas();
                                mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
                            }
                            mAuth.signOut();
                            logoutUser();
                        } else {
                            showSnack(connect);
                        }
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
                        return true;
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        lblName.setText(name);
        lblEmail.setText(email);

        String letter = "A";

        if(name != null && !name.isEmpty()) {
            letter = name.substring(0, 1);
        }

        int color = mColorGenerator.getRandomColor();
        mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
        profilImage.setImageDrawable(mDrawableBuilder);

        /*if(!session.isLoggedIn()) {
            logoutUser();
        }*/

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                        getReminderList(username);
                    }
                });
                getReminderList(username);
            }
        });

        refreshLayout.setColorSchemeResources(R.color.green_600);
    }

    private void hideMenu() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.profile).setVisible(false);
        nav_Menu.findItem(R.id.list_teman).setVisible(false);
        nav_Menu.findItem(R.id.offonmode).setVisible(false);
        nav_Menu.findItem(R.id.logout).setVisible(false);
        nav_Menu.findItem(R.id.onlineMode).setVisible(true);
    }

    private void showAbout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity2.this, R.style.AlertDialogCustom));
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_about, null);
        dialog.setIcon(R.drawable.ic_info_24dp);
        dialog.setTitle("Tentang Aplikasi");
        dialog.setView(dialogView);
        dialog.setCancelable(false);
        dialog.setPositiveButton("Rate", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }).setNegativeButton("Tutup ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Action for "Tutup".
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_long, menu);
    }

    private android.support.v7.view.ActionMode.Callback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            getMenuInflater().inflate(R.menu.menu_long, menu);
            mMultiSelector.clearSelections();

            MenuItem share = menu.findItem(R.id.share_tugas);
            if (mode.equals("offline")) {
                share.setVisible(false);
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            super.onDestroyActionMode(actionMode);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.delete_tugas:
                    actionMode.finish();
                    for (int i = IDmap.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            int id = IDmap.get(i);
                            Tugas temp = tb.getTugas(id, 0);
                            int reminder_id = temp.getReminderID();

                            String id_reminder = String.valueOf(reminder_id);
                            deleteTugas(id_reminder, temp.getID(), i);
                            //deleteReminder(id_reminder, temp.getID(), i);
                            //tb.deleteTugas(temp);
                            //mAdapter.removeItemSelected(i);
                            //mAlarmReceiver.cancelAlarm(getApplicationContext(), id);
                        }
                    }

                    mMultiSelector.clearSelections();
                    //mAdapter.onDeleteItem(getDefaultItemCount());

                    /*List<Tugas> mTest = tb.getAllTugas();

                    if (mTest.isEmpty()) {
                        mNoReminderView.setVisibility(View.VISIBLE);
                        mIcadd.setVisibility(View.VISIBLE);
                        mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
                    } else {
                        mNoReminderView.setVisibility(View.GONE);
                        mIcadd.setVisibility(View.GONE);
                        ProccessReminder pr = new ProccessReminder();
                        pr.ProccessReminder(getApplicationContext());
                    }*/

                    if (status_delete) {
                        Toast.makeText(getApplicationContext(), "Terhapus", Toast.LENGTH_SHORT).show();
                    }

                    return true;

                case R.id.share_tugas:
                    int id;
                    boolean connect = ConnectivityReceiver.isConnected();
                    if (connect) {
                        ArrayList<Integer> id_reminder = new ArrayList<Integer>();
                        for (int i = IDmap.size(); i >= 0; i--) {
                            if (mMultiSelector.isSelected(i, 0)) {
                                id = IDmap.get(i);
                                Tugas r = tb.getTugas(id, 0);
                                int reminder_id = r.getReminderID();
                                id_reminder.add(reminder_id);
                            }
                        }
                        Intent in = new Intent(MainActivity2.this, ListTemanActivity.class);
                        in.putIntegerArrayListExtra("ID", id_reminder);
                        startActivity(in);
                        actionMode.finish();
                        mMultiSelector.clearSelections();
                    } else {
                        showSnack(connect);
                    }
                    return true;

                default:
                    break;
            }
            return false;
        }
    };

    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);
        Intent i = new Intent(this, DetaiTugaslActivity.class);
        i.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, mStringClickID);
        i.putExtra("action", 1);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.setItemCount(getDefaultItemCount());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();

        List<Tugas> mTest = tb.getAllTugas();

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        } else {
            mNoReminderView.setVisibility(View.GONE);
            mIcadd.setVisibility(View.GONE);
            //ProccessReminder pr = new ProccessReminder();
            //pr.ProccessReminder(getApplicationContext());
        }

        mAdapter.setItemCount(getDefaultItemCount());

        mode = pm.getMode();
        if (mode.equals("offline")) {
            name = "MODE OFFLINE";
            lblName.setText(name);
            email = "Anda sedang dalam mode offline";
            lblEmail.setText(email);
            hideMenu();
        } else {
            HashMap<String, String> user = tb.getUserDetails();

            name = user.get("name");
            lblName.setText(name);
            email = user.get("email");
            lblEmail.setText(email);
        }

        AppController.getInstance().setConnectivityListener(this);
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        int margin = ((Number) (getResources().getDisplayMetrics().density * 65)).intValue();
        lps.setMargins(30, margin, margin, margin);
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.fabAdd), message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    public class TugasAdapter extends RecyclerView.Adapter<TugasAdapter.VerticalItemHolder> {
        private ArrayList<ReminderItem> mItems;

        long milSecond = 1000;
        long milMinute = 60000;
        long milHour = 3600000;
        long milDay = 86400000;
        long milWeek = 604800000;
        long milMonth = 2592000000L;
        long hasilPerban, hasilPerban2;

        public TugasAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
        }

        public void removeItemSelected(int selected) {
            if (!mItems.isEmpty()) {
                //mAdapter.notifyDataSetChanged();
                mItems.remove(selected);
                notifyItemRemoved(selected);
            }
        }

        @Override
        public VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_items, container, false);

            return new VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public  class ReminderItem {
            public String mTitle;
            public String mDateTime;

            public ReminderItem(String Title, String DateTime) {
                this.mTitle = Title;
                this.mDateTime = DateTime;
            }
        }

        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter)a).getDateTime();
                String o2 = ((DateTimeSorter)b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        public class VerticalItemHolder extends SwappingHolder
                implements View.OnClickListener, View.OnLongClickListener {
            private TextView mTitleText, mDateAndTimeText, mCountDown;
            //private TextView mRepeatInfoText;
            private ImageView mThumbnailImage;
            //private ImageView mActiveImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private TugasAdapter mAdapter;
            //private CircleImageView civ;

            public VerticalItemHolder(View itemView, TugasAdapter adapter) {
                super(itemView, mMultiSelector);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(true);

                mAdapter = adapter;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mCountDown = (TextView) itemView.findViewById(R.id.recycle_countdown);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    mTempPost = mList.getChildAdapterPosition(v);

                    int mReminderClickID = IDmap.get(mTempPost);
                    selectReminder(mReminderClickID);

                } else if(mMultiSelector.getSelectedPositions().isEmpty()){
                    mAdapter.setItemCount(getDefaultItemCount());
                    mMultiSelector.clearSelections();
                }
            }

            @Override
            public boolean onLongClick(View v) {
                AppCompatActivity activity = MainActivity2.this;
                activity.startSupportActionMode(mDeleteMode);
                mMultiSelector.setSelectable(true);
                mMultiSelector.setSelected(this, true);
                return true;
            }

            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if(title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            public void setReminderDateTime(String datetime) {
                Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try{
                    date2 = sdf.parse(datetime);
                } catch (ParseException e){
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                long perbandingan = (c.getTimeInMillis() - date2.getTime()) * -1;

                if (perbandingan >= milSecond && perbandingan <= milMinute) {
                    hasilPerban = perbandingan/milSecond;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Detik");
                } else if (perbandingan >= milMinute && perbandingan <= milHour) {
                    hasilPerban = perbandingan/milMinute;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Menit");
                } else if (perbandingan >= milHour && perbandingan <= milDay) {
                    hasilPerban = perbandingan/milHour;
                    hasilPerban2 = perbandingan%milHour;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Jam " + hasilPerban2/milMinute + " Menit");
                } else if (perbandingan >= milDay && perbandingan <= milWeek) {
                    hasilPerban = perbandingan / milDay;
                    hasilPerban2 = perbandingan % milDay;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Hari " + hasilPerban2 / milHour + " Jam");
                } else if (perbandingan >= milWeek && perbandingan <= milMonth){
                    hasilPerban = perbandingan / milWeek;
                    hasilPerban2 = perbandingan % milWeek;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Minggu " + hasilPerban2 / milDay + " Hari");
                } else if (perbandingan >= milMonth) {
                    hasilPerban = perbandingan / milMonth;
                    hasilPerban2 = perbandingan % milMonth;
                    mDateAndTimeText.setText(datetime + "");
                    if (hasilPerban2 >= milDay && hasilPerban2 <= milWeek) {
                        mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milDay + " Hari");
                    } else if (hasilPerban2 >= milWeek && hasilPerban2 <= milMonth) {
                        long hasilPerban3 = hasilPerban2 % milWeek;
                        if (hasilPerban3 >= milDay) {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu " + hasilPerban3 / milDay + " Hari");
                        } else {
                            mCountDown.setText(" " + hasilPerban + " Bulan " + hasilPerban2 / milWeek + " Minggu");
                        }
                    } else {
                        mCountDown.setText(" " + hasilPerban + " Bulan");
                    }
                } else {
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText("SELESAI!");
                }
            }
        }

        //testing data dami
        public  ReminderItem generateDummyData() {
            return new ReminderItem("1", "2");
        }

        public List<ReminderItem> generateData(int count) {
            ArrayList<ReminderItem> items = new ArrayList<>();

            List<Tugas> tugas = tb.getAllTugas();
            List<String> Titles = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList= new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();

            for (Tugas r : tugas) {
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDateDeadline() + " " + r.getTimeDeadline());
                IDList.add(r.getID());
            }

            int key = 0;
            for(int k = 0; k<Titles.size(); k++){
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            for (DateTimeSorter item:DateTimeSortList) {
                int i = item.getIndex();

                items.add(new TugasAdapter.ReminderItem(Titles.get(i), DateAndTime.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }

    private void deleteTugas(String reminder_id, int id, int pos) {
        if (mode.equals("online")) {
            boolean connect = ConnectivityReceiver.isConnected();
            if (connect) {
                deleteReminder(reminder_id, id, pos);
            } else {
                showSnack(connect);
            }
        } else if (mode.equals("offline")) {
            tb.deleteTugas(id);
            mAdapter.removeItemSelected(pos);
            //mAdapter.onDeleteItem(getDefaultItemCount());

            List<Tugas> mTest = tb.getAllTugas();

            if (mTest.isEmpty()) {
                mNoReminderView.setVisibility(View.VISIBLE);
                mIcadd.setVisibility(View.VISIBLE);
                mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
            } else {
                mNoReminderView.setVisibility(View.GONE);
                mIcadd.setVisibility(View.GONE);
                ProccessReminder pr = new ProccessReminder();
                pr.ProccessReminder(getApplicationContext());
            }
        }
    }

    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(MainActivity2.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveToLocal(int reminder_id, int member_id, String title, String desc, String date, String date_deadline, String time, String time_deadline, int updated) {
        int ID;
        ID = tb.addTugas(new Tugas(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated));
        if (ID == 1 || ID > 1) {
            ProccessReminder pr = new ProccessReminder();
            pr.ProccessReminder(getApplicationContext());
        }
    }

    private void syncTugas() {
        List<Tugas> tugas = tb.getAllTugas();
        if (tugas.isEmpty()) {
            getReminderList2(username);
        } else {
            for (Tugas t : tugas) {
                if (t.getReminderID() == 0) {
                    saveToServer(t,member_id);
                }
            }
        }
    }

    private void sync(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    List<Tugas> tugas = tb.getAllTugas();
                    List<Integer> id = new ArrayList<>();

                    // Check for error node in json
                    if (!error) {
                        List<Integer> updatedLocal = new ArrayList<>();
                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                id.add(t.getReminderID());
                                updatedLocal.add(t.getUpdate());
                            }
                        }

                        JSONArray tugas_arr = jObj.getJSONArray("reminder_list");
                        int l_tugas = tugas_arr.length();

                        for (int i=0; i<l_tugas; i++) {
                            JSONObject c = tugas_arr.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            if (i < id.size()) {
                                if (reminder_id == id.get(i)) {
                                    if (updatedLocal.get(i) > updated) {
                                        updateReminder(tugas.get(i), String.valueOf(member_id));
                                    }
                                }
                            }
                        }

                        List<Tugas> tugas2 = tb.getAllTugas();
                        if (tugas2.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            mAdapter.setItemCount(getDefaultItemCount());
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderList(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    List<Tugas> tugas = tb.getAllTugas();
                    List<Integer> id = new ArrayList<>();

                    // Check for error node in json
                    if (!error) {
                        List<Integer> updatedLocal = new ArrayList<>();
                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                id.add(t.getReminderID());
                                updatedLocal.add(t.getUpdate());
                            }
                        }
                        JSONArray tugas_arr = jObj.getJSONArray("reminder_list");
                        int l_tugas = tugas_arr.length();

                        for (int i=0; i<l_tugas; i++) {
                            JSONObject c = tugas_arr.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            if (i < id.size()) {
                                if (id.get(i) == reminder_id) {
                                    if (updatedLocal.get(i) > updated) {
                                        updateReminder(tugas.get(i), String.valueOf(reminder_id));
                                    }
                                } else {
                                    saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                                }
                                if (id.get(i) != reminder_id){
                                    saveToServer(tugas.get(i), String.valueOf(member_id));
                                }
                            } else {
                                saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                            }
                            if (l_tugas < id.size() && i == l_tugas-1) {
                                for (int j = i+1; j<id.size(); j++) {
                                    saveToServer(tugas.get(j), String.valueOf(member_id));
                                }
                            }
                        }

                        List<Tugas> tugas2 = tb.getAllTugas();
                        if (tugas2.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            mAdapter.setItemCount(getDefaultItemCount());
                        }

                        tugas.clear();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        if (!tugas.isEmpty()) {
                            for (Tugas t : tugas) {
                                saveToServer(t, member_id);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                refreshLayout.setRefreshing(false);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
                refreshLayout.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderList2(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        JSONArray tugas_arr = jObj.getJSONArray("reminder_list");
                        int l_tugas = tugas_arr.length();

                        for (int i=0; i<l_tugas; i++) {
                            JSONObject c = tugas_arr.getJSONObject(i);
                            int reminder_id = c.getInt("reminder_id");
                            int member_id = c.getInt("member_id");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");
                            int updated = c.getInt("updated");

                            saveToLocal(reminder_id, member_id, title, desc, date, date_deadline, time, time_deadline, updated);
                        }

                        List<Tugas> tugas = tb.getAllTugas();
                        if (tugas.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            mAdapter.setItemCount(getDefaultItemCount());
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getReminderListShared(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Get Tugas Shared Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                        for (int i = 0; i < reminder_list.length(); i++) {
                            JSONObject c = reminder_list.getJSONObject(i);

                            int reminder_id = c.getInt("reminder_id");
                            String from = c.getString("name");
                            String title = c.getString("title");
                            String desc = c.getString("desc");
                            String date = c.getString("date");
                            String date_deadline = c.getString("date_deadline");
                            String time = c.getString("time");
                            String time_deadline = c.getString("time_deadline");

                            tb.addSharedTugas(new Tugas(reminder_id, from, title, desc, date, date_deadline, time, time_deadline));
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Get Tugas Shared Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("saved", String.valueOf(1));
                params.put("synced", String.valueOf(1));

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void deleteReminder(final String reminder_id, final int id, final int pos) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Menghapus Data");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Delete Tugas Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        status_delete = true;

                        String successmsg = jObj.getString("success_msg");
                        tb.deleteTugas(id);
                        mAdapter.removeItemSelected(pos);
                        mAdapter.onDeleteItem(getDefaultItemCount());

                        List<Tugas> mTest = tb.getAllTugas();

                        if (mTest.isEmpty()) {
                            mNoReminderView.setVisibility(View.VISIBLE);
                            mIcadd.setVisibility(View.VISIBLE);
                            mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
                        } else {
                            mNoReminderView.setVisibility(View.GONE);
                            mIcadd.setVisibility(View.GONE);
                            ProccessReminder pr = new ProccessReminder();
                            pr.ProccessReminder(getApplicationContext());
                        }

                        //Toast.makeText(getApplicationContext(), successmsg, Toast.LENGTH_LONG).show();
                    } else {
                        status_delete = false;
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                status_delete = false;
                Log.e(TAG, "Delete Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi tidak stabil", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", reminder_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void resetToken(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        //pDialog.setMessage("Mencari ...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESET_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Reset Token Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        String successmsg = jObj.getString("success_msg");
                        //Toast.makeText(getApplicationContext(), successmsg, Toast.LENGTH_LONG).show();

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Reset Token Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveToServer(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_reminder";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SAVE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Save To Server Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        int reminder_id= jObj.getInt("reminder_id");

                        JSONObject user = jObj.getJSONObject("reminder");
                        int member_id = user.getInt("member_id");

                        Tugas t = tb.getTugas(tugas.getID(), 0);
                        t.setReminderID(reminder_id);
                        t.setMemberID(member_id);
                        tb.updateTugas(t);
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Save To Server Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", member_id);
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateReminder(final Tugas tugas, final String member_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Update Tugas Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        //Toast.makeText(getApplicationContext(), "Edit Berhasil", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Sync Success");
                    } else {
                        // Error in login. Get the error message
                        /*String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();*/

                        Log.e(TAG, "Sync Error");
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Update Tugas Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", String.valueOf(tugas.getReminderID()));
                params.put("title", tugas.getTitle());
                params.put("desc", tugas.getDesc());
                params.put("date", tugas.getDate());
                params.put("date_deadline", tugas.getDateDeadline());
                params.put("time", tugas.getTime());
                params.put("time_deadline", tugas.getTimeDeadline());
                params.put("member_id", member_id);
                params.put("updated", String.valueOf(tugas.getUpdate()));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
