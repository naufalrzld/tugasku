package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.List;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.helper.AlarmReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.ProccessReminder;
import fourengineering.tugasku.helper.TugasDatabase;

public class SettingActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private String mRepeat;
    private String mRepeatNo;
    private String mRepeatType;
    private String mActive;
    private Switch mRepeatSwitch, mNotifSwitch;
    private TextView mRepeatText, mRepeatNoText, mRepeatTypeText, mNotifText;
    private FloatingActionButton mFAB1, mFAB2;
    private PrefManager pm;
    private TugasDatabase tb;
    private ProccessReminder pr;
    private AlarmReceiver mAlarmReceiver;
    private long mRepeatTime;
    private List<Tugas> mList;

    public static final String EXTRA_REMINDER_ID = "1";

    private static final long milMenit = 60000L;
    private static final long milJam = 3600000L;
    private static final long milHari = 86400000L;
    private static final long milMinggu = 604800000L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        mRepeatText = (TextView) findViewById(R.id.set_repeat);
        mRepeatNoText = (TextView) findViewById(R.id.set_repeat_no);
        mRepeatTypeText = (TextView) findViewById(R.id.set_repeat_type);
        mNotifText = (TextView) findViewById(R.id.set_notif);
        mRepeatSwitch = (Switch) findViewById(R.id.repeat_switch);
        mNotifSwitch = (Switch) findViewById(R.id.notif_switch);
        //mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
        //mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mAlarmReceiver = new AlarmReceiver();

        pr = new ProccessReminder();

        tb = new TugasDatabase(getApplicationContext());
        mList = tb.getAllTugas();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_activity_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mRepeatNo = Integer.toString(1);

        pm = new PrefManager(this);
        mRepeat = pm.getRepeat();
        mRepeatNo = pm.getRepeatNo();
        mRepeatType = pm.getRepeatType();
        mActive = pm.getActive();

        mRepeatNoText.setText(mRepeatNo);
        mRepeatTypeText.setText(mRepeatType);
        mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType);

        if (mActive.equals("false")) {
            //mFAB1.setVisibility(View.VISIBLE);
            //mFAB2.setVisibility(View.GONE);
            mNotifSwitch.setChecked(false);
            mNotifText.setText(R.string.notif_off);
        } else if (mActive.equals("true")) {
            //mFAB1.setVisibility(View.GONE);
            //mFAB2.setVisibility(View.VISIBLE);
            mNotifSwitch.setChecked(true);
            mNotifText.setText(R.string.notif_on);
        }

        if (mRepeat.equals("false")) {
            mRepeatSwitch.setChecked(false);
            mRepeatText.setText(R.string.repeat_off);
        } else if (mRepeat.equals("true")) {
            mRepeatSwitch.setChecked(true);
        }
        /*if (!mTest.isEmpty()){
            ProccessReminder pr = new ProccessReminder();
            pr.ProccessReminder(getApplicationContext());
        } else if (mTest.isEmpty()) {
            mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
        }*/
    }

    public void selectRepeatType(View v){
        final String[] items = new String[4];

        items[0] = "Menit";
        items[1] = "Jam";
        items[2] = "Hari";
        items[3] = "Minggu";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pilih Tipe");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                mRepeatType = items[item];
                pm.setRepeatType(mRepeatType);
                mRepeatTypeText.setText(mRepeatType);
                mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void setRepeatNo(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Masukkan Angka");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(input);
        alert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        if (input.getText().toString().length() == 0) {
                            mRepeatNo = Integer.toString(1);
                            mRepeatNoText.setText(mRepeatNo);
                            mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType);
                        } else {
                            mRepeatNo = input.getText().toString().trim();
                            pm.setRepeatNo(mRepeatNo);
                            mRepeatNoText.setText(mRepeatNo);
                            mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType);
                        }
                    }
                });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void onSwitchNotif(View view) {
        //boolean on = ((Switch) view).isChecked();
        boolean on = mNotifSwitch.isChecked();

        if (on) {
            mActive = "true";
            pm.setActive(mActive);
            mNotifText.setText(R.string.notif_on);
        } else {
            mActive = "false";
            pm.setActive(mActive);
            mNotifText.setText(R.string.notif_off);
        }
    }

    public void onSwitchRepeat(View view) {
        //boolean on = ((Switch) view).isChecked();
        boolean on = mRepeatSwitch.isChecked();

        if (on) {
            mRepeat = "true";
            pm.setRepeat(mRepeat);
            mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType);
        } else {
            mRepeat = "false";
            pm.setRepeat(mRepeat);
            mRepeatText.setText(R.string.repeat_off);
        }
    }

    /*public void selectFab1(View v) {
        mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
        mFAB1.setVisibility(View.GONE);
        mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
        mFAB2.setVisibility(View.VISIBLE);
        mActive = "true";
        pm.setActive(mActive);
        mNotifText.setText(R.string.notif_on);
    }

    public void selectFab2(View v) {
        mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
        mFAB2.setVisibility(View.GONE);
        mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
        mFAB1.setVisibility(View.VISIBLE);
        mActive = "false";
        pm.setActive(mActive);
        mNotifText.setText(R.string.notif_off);
    }*/

    public void processReminder() {

        if (mRepeatType.equals("Menit")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milMenit;
        } else if (mRepeatType.equals("Jam")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milJam;
        } else if (mRepeatType.equals("Hari")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milHari;
        } else if (mRepeatType.equals("Minggu")) {
            mRepeatTime = Integer.parseInt(mRepeatNo) * milMinggu;
        }

        if (mActive.equals("true")) {
            if (mRepeat.equals("true")) {
                //new AlarmReceiver().setRepeatAlarm(getApplicationContext(), mCalendar, date2.getTime(), ID, mRepeatTime);
                mAlarmReceiver.setRepeatAlarm(getApplicationContext(), 1, mRepeatTime);
            } else if (mRepeat.equals("false")) {
                //new AlarmReceiver().setAlarm(getApplicationContext(), mCalendar, ID);
            }
        }

        //Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!mList.isEmpty()){
            pr.ProccessReminder(getApplicationContext());
        } /*else if (mList.isEmpty()) {
            mAlarmReceiver.cancelAlarm(getApplicationContext(), 1);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
