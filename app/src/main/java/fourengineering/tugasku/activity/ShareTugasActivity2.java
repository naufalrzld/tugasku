package fourengineering.tugasku.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.amlcurran.showcaseview.ShowcaseView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.adapter.Tugas;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.DateTimeSorter;
import fourengineering.tugasku.helper.TugasDatabase;

public class ShareTugasActivity2 extends AppCompatActivity implements
        com.github.amlcurran.showcaseview.OnShowcaseEventListener {
    private static final String TAG = ShareTugasActivity2.class.getSimpleName();

    private RecyclerView mList;
    private SimpleAdapter mAdapter;
    private Toolbar mToolbar;
    private TextView mNoReminderView;
    private ImageView mIcadd;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private TugasDatabase tb;
    private MultiSelector mMultiSelector = new MultiSelector();
    //private AlarmReceiver mAlarmReceiver;
    private ShowcaseView sv;
    private ProgressDialog pDialog;
    private String username;
    private Tugas reminder = new Tugas();
    private String mTitle;
    private int mTempPost;
    //CircleImageView mTitleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share2);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        tb = new TugasDatabase(getApplicationContext());

        HashMap<String, String> user = tb.getUserDetails();
        username = user.get("username");
        //getReminderListShared(username);
        checkLimit(username);

        List<Tugas> test = tb.getAllTugasShared();
        List<Integer> id = new ArrayList<>();
        List<String> title = new ArrayList<>();
        for (Tugas r : test) {
            id.add(r.getReminderID());
        }

        for (int i=0; i<id.size(); i++) {
            checkReminder(id.get(i).toString());
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mList = (RecyclerView) findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) findViewById(R.id.no_reminder_text);
        mIcadd = (ImageView) findViewById(R.id.ic_add);
        List<Tugas> mTest = tb.getAllTugasShared();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.title_activity_share);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        }

        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new SimpleAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        //mAlarmReceiver = new AlarmReceiver();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_long, menu);
    }

    private android.support.v7.view.ActionMode.Callback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            getMenuInflater().inflate(R.menu.menu_long, menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.discard_reminder:
                    actionMode.finish();
                    for (int i = IDmap.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) {
                            int id = IDmap.get(i);
                            Tugas temp = tb.getTugasShared(0,id);

                            tb.deleteTugasShared(temp);
                            mAdapter.removeItemSelected(i);
                            //mAlarmReceiver.cancelAlarm(getApplicationContext(), id);

                        }
                    }

                    mMultiSelector.clearSelections();
                    mAdapter.onDeleteItem(getDefaultItemCount());
                    Toast.makeText(getApplicationContext(),
                            "Deleted",
                            Toast.LENGTH_SHORT).show();

                    List<Tugas> mTest = tb.getAllTugasShared();

                    if (mTest.isEmpty()) {
                        mNoReminderView.setVisibility(View.VISIBLE);
                        mIcadd.setVisibility(View.VISIBLE);
                    } else {
                        mNoReminderView.setVisibility(View.GONE);
                        mIcadd.setVisibility(View.GONE);
                    }

                    return true;

                case R.id.save_reminder:
                    actionMode.finish();
                    mMultiSelector.clearSelections();
                    return true;

                default:
                    break;
            }
            return false;
        }
    };

    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);
        Intent i = new Intent(this, DetaiTugaslActivity.class);
        i.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, mStringClickID);
        i.putExtra("action", 2);
        startActivityForResult(i, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.setItemCount(getDefaultItemCount());
    }

    @Override
    public void onResume() {
        super.onResume();

        List<Tugas> mTest = tb.getAllTugasShared();

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
            mIcadd.setVisibility(View.VISIBLE);
        } else {
            mNoReminderView.setVisibility(View.GONE);
            mIcadd.setVisibility(View.GONE);
        }

        mAdapter.setItemCount(getDefaultItemCount());
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.VerticalItemHolder> {
        public ArrayList<ReminderItem> mItems;

        long milSecond = 1000;
        long milMinute = 60000;
        long milHour = 3600000;
        long milDay = 86400000;
        long milWeek = 604800000;
        long hasilPerban, hasilPerban2;

        public SimpleAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
        }

        public void removeItemSelected(int selected) {
            if (mItems.isEmpty()) return;
            mItems.remove(selected);
            notifyItemRemoved(selected);
        }

        @Override
        public VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_item_shared, container, false);

            return new VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime);
            itemHolder.setReminderFrom(item.mFrom);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class ReminderItem {
            public String mFrom;
            public String mTitle;
            public String mDateTime;

            public ReminderItem(String From, String Title, String DateTime) {
                this.mFrom = From;
                this.mTitle = Title;
                this.mDateTime = DateTime;
            }
        }

        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter) a).getDateTime();
                String o2 = ((DateTimeSorter) b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        public class VerticalItemHolder extends SwappingHolder
                implements View.OnClickListener, View.OnLongClickListener {
            private TextView mTitleText, mFromText, mDateAndTimeText, /*mRepeatInfoText,*/ mCountDown;
            private ImageView mThumbnailImage;
            //private ImageView mActiveImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private SimpleAdapter mAdapter;
            private CheckBox mCB;

            public VerticalItemHolder(View itemView, SimpleAdapter adapter) {
                super(itemView, mMultiSelector);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(false);

                mAdapter = adapter;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mFromText = (TextView) itemView.findViewById(R.id.info_share);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mCountDown = (TextView) itemView.findViewById(R.id.recycle_countdown);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
                mCB = (CheckBox) itemView.findViewById(R.id.checkbox);

            }

            @Override
            public void onClick(View v) {
                if (!mMultiSelector.tapSelection(this)) {
                    mTempPost = mList.getChildAdapterPosition(v);

                    int mReminderClickID = IDmap.get(mTempPost);
                    selectReminder(mReminderClickID);

                } else if (mMultiSelector.getSelectedPositions().isEmpty()) {
                    mAdapter.setItemCount(getDefaultItemCount());
                }
            }

            @Override
            public boolean onLongClick(View v) {
                return false;
            }

            public void setReminderFrom(String from) {
                mFromText.setText("Dari " + from);
            }

            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if (title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            public void setReminderDateTime(String datetime) {
                Date date2 = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try {
                    date2 = sdf.parse(datetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c = Calendar.getInstance();
                long perbandingan = (c.getTimeInMillis() - date2.getTime()) * -1;
                if (perbandingan >= milSecond && perbandingan <= milMinute) {
                    hasilPerban = perbandingan / milSecond;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Detik");
                } else if (perbandingan >= milMinute && perbandingan <= milHour) {
                    hasilPerban = perbandingan / milMinute;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Menit");
                } else if (perbandingan >= milHour && perbandingan <= milDay) {
                    hasilPerban = perbandingan / milHour;
                    hasilPerban2 = perbandingan % milHour;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Jam " + hasilPerban2 / milMinute + " Menit");
                } else if (perbandingan >= milDay && perbandingan <= milWeek) {
                    hasilPerban = perbandingan / milDay;
                    hasilPerban2 = perbandingan % milDay;
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText(" " + hasilPerban + " Hari " + hasilPerban2 / milHour + " Jam");
                } else {
                    mDateAndTimeText.setText(datetime + "");
                    mCountDown.setText("SELESAI!");
                }
            }
        }

        //testing data dami
        public ReminderItem generateDummyData() {
            return new ReminderItem("1", "2", "3");
        }

        public List<ReminderItem> generateData(int count) {
            ArrayList<ReminderItem> items = new ArrayList<>();

            List<Tugas> tugas = tb.getAllTugasShared();
            List<String> From = new ArrayList<>();
            List<String> Titles = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList = new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();

            for (Tugas r : tugas) {
                From.add(r.getFrom());
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDateDeadline() + " " + r.getTimeDeadline());
                IDList.add(r.getID());
            }

            int key = 0;
            for (int k = 0; k < Titles.size(); k++) {
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            for (DateTimeSorter item : DateTimeSortList) {
                int i = item.getIndex();

                items.add(new SimpleAdapter.ReminderItem(From.get(i), Titles.get(i), DateAndTime.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getReminderListShared(final String username, final String limit) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Memuat Data ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    JSONArray reminder_list = jObj.getJSONArray("reminder_list");

                    /*int a = reminder_list.length() - test.size();
                    selisih.add(String.valueOf(a));
                    Toast.makeText(getApplicationContext(), selisih.get(0), Toast.LENGTH_LONG).show();*/

                    //if (test.size() < reminder_list.length()) {
                    // Check for error node in json
                        if (!error) {

                        //JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                            for (int i = 0; i < reminder_list.length(); i++) {
                                JSONObject c = reminder_list.getJSONObject(i);
                                int reminder_id = c.getInt("reminder_id");
                                String from = c.getString("name");
                                String title = c.getString("title");
                                String desc = c.getString("desc");
                                String date = c.getString("date");
                                String date_deadline = c.getString("date_deadline");
                                String time = c.getString("time");
                                String time_deadline = c.getString("time_deadline");

                                tb.addSharedTugas(new Tugas(reminder_id, from, title, desc, date, date_deadline, time, time_deadline));
                            }
                            onResume();
                        } else {
                            // Error in login. Get the error message
                            String errorMsg = jObj.getString("error_msg");
                            //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();

                        //}
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("limit", limit);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void checkLimit(final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Checking...");
        //showDialog();

        final List<Tugas> test = tb.getAllTugasShared();
        final List<Integer> id = new ArrayList<>();
        for (Tugas r : test) {
            id.add(r.getID());
        }

        //final List<String> selisih = new ArrayList<>();
        final String[] selisih1 = {"100"};

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETREMINDERLISTSHARED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    //JSONArray reminder_list = jObj.getJSONArray("reminder_list");

                        // Check for error node in json
                    if (!error) {
                        JSONArray reminder_list = jObj.getJSONArray("reminder_list");
                        if (test.size() < reminder_list.length()) {
                            int a = reminder_list.length() - test.size();
                            String selisih = String.valueOf(a);
                            //selisih.add(String.valueOf(a));
                            //Toast.makeText(getApplicationContext(), selisih, Toast.LENGTH_LONG).show();
                                getReminderListShared(username, selisih);

                            /*for (int i = 0; i < reminder_list.length(); i++) {
                                JSONObject c = reminder_list.getJSONObject(i);
                                int reminder_id = c.getInt("reminder_id");
                                String from = c.getString("name");
                                String title = c.getString("title");
                                String date = c.getString("date");
                                String date_deadline = c.getString("date_deadline");
                                String time = c.getString("time");
                                String time_deadline = c.getString("time_deadline");
                                String voice = c.getString("voice");
                                String photo = c.getString("photo");
                                String type = c.getString("type");

                                //rb.addSharedReminder(new Reminder(reminder_id, from, title, date, date_deadline, time, time_deadline, voice, photo, type));
                            }*/
                        }
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        mNoReminderView.setText(errorMsg);
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("limit", selisih1[0]);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void checkReminder(final String reminder_id) {
        // Tag used to cancel the request
        String tag_string_req = "req_search";

        pDialog.setMessage("Mencari ...");
        //showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_CHECKREMINDER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response.toString());
                //hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    /*if (!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }*/

                    if (!error) {
                        String errorMsg = jObj.getString("error_msg");
                        //String reminder_id_ = jObj.getString("reminder_id");
                        Tugas temp = tb.getTugasShared(Integer.parseInt(reminder_id), 0);
                        tb.deleteTugasShared(temp);
                        //Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                        onResume();
                    } else {
                        String errorMsg = jObj.getString("error_msg");
                        String reminder_id_ = jObj.getString("reminder_id");
                        String title = jObj.getString("title");
                        String date = jObj.getString("date");
                        String date_deadline = jObj.getString("date_deadline");
                        String time = jObj.getString("time");
                        String time_deadline = jObj.getString("time_deadline");

                        reminder = tb.getTugasShared(Integer.valueOf(reminder_id), 0);

                        String title_ = reminder.getTitle();

                        if (!title.equals(title_)) {
                            reminder.setReminderID(Integer.valueOf(reminder_id_));
                            reminder.setTitle(title);
                            reminder.setDate(date);
                            reminder.setDateDeadline(date_deadline);
                            reminder.setTime(time);
                            reminder.setTimeDeadline(time_deadline);

                            tb.updateTugasShared(reminder);
                            onResume();
                        }

                        //reminder.setReminderID(Integer.valueOf(reminder_id));
                        //reminder.setTitle(title);

                        //rb.updateReminderShared(reminder);

                        //Toast.makeText(getApplicationContext(), rb.updateReminderShared(reminder) + " " + title, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Search Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reminder_id", reminder_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
