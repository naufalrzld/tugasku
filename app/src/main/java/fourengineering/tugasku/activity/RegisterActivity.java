package fourengineering.tugasku.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import fourengineering.tugasku.R;
import fourengineering.tugasku.app.AppConfig;
import fourengineering.tugasku.app.AppController;
import fourengineering.tugasku.helper.ConnectivityReceiver;
import fourengineering.tugasku.helper.PrefManager;
import fourengineering.tugasku.helper.SessionManager;
import fourengineering.tugasku.helper.SharedPrefManager;

public class RegisterActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    EditText mUsernameText, mPasswordText, mFullNameText, mEmailText;
    Button mRegisterBtn, mLinkToLoginBtn;
    private CheckBox lihat_pass;

    private FirebaseAuth mAuth;

    private ProgressBar progressBar;

    private SessionManager session;
    private PrefManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        checkConnection();

        mAuth = FirebaseAuth.getInstance();

        mFullNameText = (EditText) findViewById(R.id.name);
        mUsernameText = (EditText) findViewById(R.id.username);
        mEmailText = (EditText) findViewById(R.id.email);
        mPasswordText = (EditText) findViewById(R.id.password);
        mRegisterBtn = (Button) findViewById(R.id.btnRegister);
        mLinkToLoginBtn = (Button) findViewById(R.id.btnLinkToLoginScreen);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        lihat_pass = (CheckBox) findViewById(R.id.cb_lihat_pwd);

        pm = new PrefManager(this);
        session = new SessionManager(this);

        lihat_pass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    mPasswordText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    mPasswordText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isConnected = ConnectivityReceiver.isConnected();
                String token = SharedPrefManager.getInstance(getApplicationContext()).getDeviceToken();
                String name = mFullNameText.getText().toString().trim();
                String username = mUsernameText.getText().toString().trim();
                String email = mEmailText.getText().toString().trim();
                String password = mPasswordText.getText().toString().trim();
                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (TextUtils.isEmpty(name)) {
                    mFullNameText.setError("Masukkan Nama Lengkap Anda!");
                    return;
                }

                if (TextUtils.isEmpty(username)) {
                    mUsernameText.setError("Masukkan Username Anda!");
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    mEmailText.setError("Masukkan Email Anda!");
                    return;
                }

                if (!email.matches(emailPattern)) {
                    mEmailText.setError("Alamat Email Tidak Valid!");
                    return;
                }

                if (password.length() < 8) {
                    mPasswordText.setError("Password Minimal 8 Karakter!");
                    return;
                }

                if (isConnected) {
                    progressBar.setVisibility(View.VISIBLE);
                    registerUser(username, token, name, email, password);
                } else {
                    showSnack(isConnected);
                }
            }
        });

        mLinkToLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Tidak Terhubung ke Internet!";
            color = Color.RED;

            Snackbar snackbar = Snackbar.make(findViewById(R.id.progressBar), message, Snackbar.LENGTH_INDEFINITE).setAction("MODE OFFLINE", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertOfflineMode();
                }
            });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    private void showAlertOfflineMode() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(RegisterActivity.this, R.style.AlertDialogCustom));
        dialog.setCancelable(false);
        dialog.setIcon(R.drawable.ic_offline_mode);
        dialog.setTitle("MODE OFFLINE");
        dialog.setMessage(Html.fromHtml("<font color='#ffffff'>Tidak ada koenksi internet? Silahkan klik \"Lanjutkan\" bila ingin mengaktifkan mode offline!</font>"));
        dialog.setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                pm.setMode("offline");
                session.setLogin(true);
                startActivity(new Intent(RegisterActivity.this, MainActivity2.class));
                finish();
            }
        }).setNegativeButton("Batal ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Action for "Cancel".
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void registerUser(final String username, final String token, final String name, final String email,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // User successfully stored in MySQL
                        mAuth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        // If sign in fails, display a message to the user. If sign in succeeds
                                        // the auth state listener will be notified and logic to handle the
                                        // signed in user can be handled in the listener.
                                        progressBar.setVisibility(View.GONE);
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(getApplicationContext(), "Pendaftaran Gagal." + task.getException(),
                                                    Toast.LENGTH_LONG).show();
                                        } else if (task.isSuccessful()) {
                                            Toast.makeText(getApplicationContext(), "Registrasi Berhasil, Silahkan Login", Toast.LENGTH_LONG).show();

                                            // Launch login activity
                                            Intent intent = new Intent(
                                                    RegisterActivity.this,
                                                    LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                });
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Koneksi anda tidak stabil atau tidak terhubung ke internet", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("token", token);
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
