package fourengineering.tugasku.app;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import fourengineering.tugasku.activity.DetaiTugaslActivity;
import fourengineering.tugasku.activity.TemanReqListActivity;

/**
 * Created by naufa on 29/12/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload : " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                sendPushNotification(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception : " + e.getMessage());
            }
        }
    }

    //this method will display the notification
    //We are passing the JSONObject that is received from
    //firebase cloud messaging
    private void sendPushNotification(JSONObject json) {
        //optionally we can display the json into log
        Log.e(TAG, "Notification JSON " + json.toString());
        try {
            //getting the json data
            JSONObject data = json.getJSONObject("data");

            //parsing json data
            String title = data.getString("title");
            String message = data.getString("message");
            String tugas_id = data.getString("tugas_id");

            //creating MyNotificationManager object
            MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());

            //creating an intent for the notification
            if (title.equals("Teman")) {
                Intent intent = new Intent(getApplicationContext(), TemanReqListActivity.class);
                mNotificationManager.showSmallNotification(title, message, intent);
            } else if (title.equals("Tugas")) {
                Intent intent = new Intent(getApplicationContext(), DetaiTugaslActivity.class);
                intent.putExtra("action", 4);
                intent.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, tugas_id);
                mNotificationManager.showSmallNotification(title, message, intent);
            } else if (title.equals("Pembaharuan")) {
                Intent intent = new Intent(getApplicationContext(), DetaiTugaslActivity.class);
                intent.putExtra("action", 5);
                intent.putExtra(DetaiTugaslActivity.EXTRA_TUGAS_ID, tugas_id);
                mNotificationManager.showSmallNotification(title, message, intent);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }
}
