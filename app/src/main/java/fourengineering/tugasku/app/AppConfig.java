package fourengineering.tugasku.app;

public class AppConfig {
	// Server user login url
	static String ip = "http://tugasku.fourengineering.com";
	public static String URL_LOGIN = ip+"/login2.php";

	// Server user register url
	public static String URL_REGISTER = ip+"/register.php";

	public static String URL_SAVE = ip+"/save.php";
	public static String URL_GETUNAME = ip+"/search_username.php";
	public static String URL_UPDATEMEMBER = ip+"/updateMember.php";
	public static String URL_ADDFRIEND = ip+"/addFriend.php";
	public static String URL_GETFRIENDLIST = ip+"/getFriend.php";
	public static String URL_GETDETILFRIEND = ip+"/getTeman.php";
	public static String URL_SHARE = ip+"/shareReminder.php";
	public static String URL_GETREMINDERLIST = ip+"/getReminderList.php";
	public static String URL_GETREMINDERLISTSHARED = ip+"/getSharedReminderList.php";
	public static String URL_DELETEREMINDER = ip+"/deleteReminder.php";
	public static String URL_UPDATEREMINDER = ip+"/updateReminder.php";
	public static String URL_CHECKREMINDER = ip+"/checkReminder.php";
	public static String URL_GETFRIENDREQCOUNT = ip+"/friend_req_count.php";
	public static String URL_CHANGEPASSWORD = ip+"/updatePassword.php";
	public static String URL_DELETEFRIEND = ip+"/deleteFriend.php";
	public static String URL_REQFRIEND = ip+"/getFriendListReq.php";
	public static String URL_ACCORDC = ip+"/acceptOrDeclineFriendReq.php";
	public static String URL_RESET_TOKEN = ip+"/resetToken.php";
	public static String URL_SEND_SINGLE_PUSH = ip+"/sendSinglePush.php";
	public static String URL_SAVE_TO_LOCAL = ip+"/saveToLocal.php";
	public static String URL_SYNC_TUGAS_SHARED = ip+"/syncTugasShared.php";
	public static String URL_GET_TUGAS_SHARED = ip+"/getTugasShared.php";
}
